var $ = jQuery.noConflict();
jQuery.fn.exists = function(callback) {
  var args = [].slice.call(arguments, 1);
  if (this.length) {
    callback.call(this, args);
  }
  return this;
};

/*----------------------------------------------------
/* Scroll to top
/*--------------------------------------------------*/
jQuery(document).ready(function() {
    //START -- MOVE-TO-TOP ARROW
	//move-to-top arrow
	jQuery("body").prepend("<div id='move-to-top' class='animate '><i class='fa fa-chevron-up'></i></div>");
	var scrollDes = 'html,body';  
	/*Opera does a strange thing if we use 'html' and 'body' together so my solution is to do the UA sniffing thing*/
	if(navigator.userAgent.match(/opera/i)){
		scrollDes = 'html';
	}
	//show ,hide
	jQuery(window).scroll(function () {
		if (jQuery(this).scrollTop() > 160) {
			jQuery('#move-to-top').addClass('filling').removeClass('hiding');
		} else {
			jQuery('#move-to-top').removeClass('filling').addClass('hiding');
		}
	});
	// scroll to top when click 
	jQuery('#move-to-top').click(function () {
		jQuery(scrollDes).animate({ 
			scrollTop: 0
		},{
			duration :500
		});
	});
	//END -- MOVE-TO-TOP ARROW
});

/*----------------------------------------------------
/* Smooth Scrolling for Anchor Tag like #comments
/*--------------------------------------------------*/
jQuery(document).ready(function($) {
    $('a[href*=#comments]:not([href=#comments])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

/*----------------------------------------------------
/* Responsive Navigation
/*--------------------------------------------------*/
jQuery(document).ready(function($){
    var menu_wrapper = $('.secondary-navigation')
    .clone().attr('class', 'mobile-menu')
    .wrap('<div id="mobile-menu-wrapper" />').parent().hide()
    .appendTo('body');

    $('.toggle-mobile-menu').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#mobile-menu-wrapper').show();
        $('body').toggleClass('mobile-menu-active');
    });
    
    // prevent propagation of scroll event to parent
    $(document).on('DOMMouseScroll mousewheel', '#mobile-menu-wrapper', function(ev) {
        var $this = $(this),
            scrollTop = this.scrollTop,
            scrollHeight = this.scrollHeight,
            height = $this.height(),
            delta = (ev.type == 'DOMMouseScroll' ?
                ev.originalEvent.detail * -40 :
                ev.originalEvent.wheelDelta),
            up = delta > 0;
    
        var prevent = function() {
            ev.stopPropagation();
            ev.preventDefault();
            ev.returnValue = false;
            return false;
        }
    
        if (!up && -delta > scrollHeight - height - scrollTop) {
            // Scrolling down, but this will take us past the bottom.
            $this.scrollTop(scrollHeight);
            return prevent();
        } else if (up && delta > scrollTop) {
            // Scrolling up, but this will take us past the top.
            $this.scrollTop(0);
            return prevent();
        }
    });
}).click(function() {
    jQuery('body').removeClass('mobile-menu-active');
});


/*----------------------------------------------------
/*  Dropdown menu
/* ------------------------------------------------- */
jQuery(document).ready(function() { 
	$('.main-header ul.sub-menu').hide(); 
    $('.main-header li').hover( 
		function() {
			$(this).children('ul.sub-menu').slideDown('fast');
		}, 
		function() {
			$(this).children('ul.sub-menu').hide();
		}
	);
});    

/*----------------------------------------------------
/* Scroll to top footer link script
/*--------------------------------------------------*/
jQuery(document).ready(function(){
    jQuery('a[href=#top]').click(function(){
        jQuery('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });
});

/*----------------------------------------------------
/* Social button scripts
/*---------------------------------------------------*/
jQuery(document).ready(function(){
	jQuery.fn.exists = function(callback) {
	  var args = [].slice.call(arguments, 1);
	  if (this.length) {
		callback.call(this, args);
	  }
	  return this;
	};
	(function(d, s) {
	  var js, fjs = d.getElementsByTagName(s)[0], load = function(url, id) {
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.src = url; js.id = id;
		fjs.parentNode.insertBefore(js, fjs);
	  };
	jQuery('span.facebookbtn, .facebook_like').exists(function() {
	  load('//connect.facebook.net/en_US/all.js#xfbml=1', 'fbjssdk');
	});
	jQuery('span.gplusbtn').exists(function() {
	  load('https://apis.google.com/js/plusone.js', 'gplus1js');
	});
	jQuery('span.twitterbtn').exists(function() {
	  load('//platform.twitter.com/widgets.js', 'tweetjs');
	});
	jQuery('span.linkedinbtn').exists(function() {
	  load('//platform.linkedin.com/in.js', 'linkedinjs');
	});
	jQuery('span.pinbtn').exists(function() {
	  load('//assets.pinterest.com/js/pinit.js', 'pinterestjs');
	});
	jQuery('span.stumblebtn').exists(function() {
	  load('//platform.stumbleupon.com/1/widgets.js', 'stumbleuponjs');
	});
	}(document, 'script'));
});

/*----------------------------------------------------
/* Header Search
/*---------------------------------------------------*/
jQuery(document).ready(function($){
    var $header = $('#header');
    var $input = $header.find('.hideinput, .search-top .fa-search');
	$header.find('.fa-search').hover(function(e){
        $input.addClass('active').focus();
	}, function() {
	   
	}).click(function() {
		if ($input.first().val()){
			$input.first().closest('form').submit();
			return false;
		}
	});
    $('.search-top .hideinput').click(function(e) {
        //e.preventDefault();
        e.stopPropagation();
    });
}).click(function(e) {
    $('#header .hideinput, .search-top .fa-search').removeClass('active');
});

// Fix: submit search on icon click
jQuery(document).ready(function($){
    $('.search-form i.fa-search').click(function() {
        $(this).closest('form').submit();
    });
    $('.fa-search').click(function() {
    	event.preventDefault();
    });
});

/*----------------------------------------------------
/* WooCommerce Image Fliper
/*---------------------------------------------------*/
jQuery(document).ready(function($){
	jQuery( 'ul.products li.pif-has-gallery a:first-child' ).hover( function() {
		jQuery( this ).children( '.wp-post-image' ).removeClass( 'fadeInDown' ).addClass( 'animated fadeOutUp' );
		jQuery( this ).children( '.secondary-image' ).removeClass( 'fadeOutUp' ).addClass( 'animated fadeInDown' );
	}, function() {
		jQuery( this ).children( '.wp-post-image' ).removeClass( 'fadeOutUp' ).addClass( 'fadeInDown' );
		jQuery( this ).children( '.secondary-image' ).removeClass( 'fadeInDown' ).addClass( 'fadeOutUp' );
	});
});

/*----------------------------------------------------
/* Stiky Nav
/*---------------------------------------------------*/
jQuery(document).ready(function() {
	jQuery('#sticky').exists(function() {
		function isScrolledTo(elem) {
			var docViewTop = jQuery(window).scrollTop(); //num of pixels hidden above current screen
			var docViewBottom = docViewTop + jQuery(window).height();

			var elemTop = jQuery(elem).offset().top; //num of pixels above the elem
			var elemBottom = elemTop + jQuery(elem).height();

			return ((elemTop <= docViewTop));
		}

		var sticky = jQuery('#sticky');
		
		jQuery(window).scroll(function(e) {
			if(isScrolledTo(sticky)) {
				sticky.css({'position': 'fixed', 'top': '0'});
			}
			if ($(window).scrollTop() == 0) {
				sticky.css({'position': 'relative', 'top': 'auto'});
			}
		});
	});
});