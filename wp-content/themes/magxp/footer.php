<?php $mts_options = get_option('magxp');
      $top_footer_num = (!empty($mts_options['mts_top_footer_num']) && $mts_options['mts_top_footer_num'] == 4) ? 4 : 3;
      $bottom_footer_num = (!empty($mts_options['mts_bottom_footer_num']) && $mts_options['mts_bottom_footer_num'] == 4) ? 4 : 3; ?>
	</div><!--#page-->
</div><!--.main-container-->
    
<footer>
    <?php if (!empty($mts_options['mts_top_footer'])) { ?>
	<div class="container">
		<div class="footer-widgets top-footer-widgets widgets-<?php echo $top_footer_num; ?>-columns clearfix">
			<div class="f-widget f-widget-1">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top') ) : ?><?php endif; ?>
			</div>
			<div class="f-widget f-widget-2">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top-2') ) : ?><?php endif; ?>
			</div>
			<div class="f-widget f-widget-3 <?php echo ($top_footer_num == 3) ? 'last' : ''; ?>">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top-3') ) : ?><?php endif; ?>
			</div>
            <?php if ($top_footer_num == 4) : ?>
            <div class="f-widget f-widget-4 last">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top-4') ) : ?><?php endif; ?>
			</div>
            <?php endif; ?>
		</div><!--.top-footer-widgets-->
    </div><!--.container-->
    <?php } ?>
    <?php if (!empty($mts_options['mts_bottom_footer'])) { ?>
    <!-- footer-widgets-second -->
    <div class="footer-widgets bottom-footer-widgets widgets-<?php echo $bottom_footer_num;?>-columns clearfix">
        <div class="container">
			<div class="f-widget f-widget-1">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-bottom') ) : ?><?php endif; ?>
			</div>
			<div class="f-widget f-widget-2">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-bottom-2') ) : ?><?php endif; ?>
			</div>
			<div class="f-widget f-widget-3 <?php echo ($bottom_footer_num == 3) ? 'last' : ''; ?>">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-bottom-3') ) : ?><?php endif; ?>
			</div>
            <?php if ($bottom_footer_num == 4) : ?>
            <div class="f-widget f-widget-4 last">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-bottom-4') ) : ?><?php endif; ?>
			</div>
            <?php endif; ?>
        </div><!--.container-->
    </div><!--.bottom-footer-widgets-->
    <?php } ?>
    <div class="copyrights">
        <?php mts_copyrights_credit(); ?>
	</div> 
</footer><!--footer-->
<?php mts_footer(); ?>
<?php wp_footer(); ?>
</div><!--.main-container-wrap-->
</body>
</html>