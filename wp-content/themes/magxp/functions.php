<?php
/*-----------------------------------------------------------------------------------*/
/*  Do not remove these lines, sky will fall on your head.
/*-----------------------------------------------------------------------------------*/
require_once( dirname( __FILE__ ) . '/theme-options.php' );
if ( ! isset( $content_width ) ) $content_width = 1060;

/*-----------------------------------------------------------------------------------*/
/*  Load Options
/*-----------------------------------------------------------------------------------*/
$mts_options = get_option('magxp');

/*-----------------------------------------------------------------------------------*/
/*  Load Translation Text Domain
/*-----------------------------------------------------------------------------------*/
load_theme_textdomain( 'mythemeshop', get_template_directory().'/lang' );

// Custom translations
if (!empty($mts_options['translate'])) {
    $mts_translations = get_option('mts_translations_'.'magxp');//$mts_options['translations'];
    function mts_custom_translate( $translated_text, $text, $domain ) {
        if ($domain == 'mythemeshop' || $domain == 'nhp-opts') {
            // get options['translations'][$text] and return value
            global $mts_translations;
            
            if (!empty($mts_translations[$text])) {
                $translated_text = $mts_translations[$text];
            }
        }
        return $translated_text;
        
    }
    add_filter( 'gettext', 'mts_custom_translate', 20, 3 );
}

if ( function_exists('add_theme_support') ) add_theme_support('automatic-feed-links');

/*-----------------------------------------------------------------------------------*/
/*  Post Thumbnail Support
/*-----------------------------------------------------------------------------------*/
if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 203, 150, true );
    add_image_size( 'featured', 203, 150, true ); //featured and related
    add_image_size( 'featured-blog', 634, 280, true ); //featured (blog layout)
    add_image_size( 'widgetthumb', 65, 65, true ); //widget
    add_image_size( 'widgetbigthumb', 300, 180, true ); //widget Big Thumb
    add_image_size( 'slider', 960, 472, true ); //slider
    add_image_size( 'sliderthumb', 124, 74, true ); //slider thumbnails
    add_image_size( 'widgetsliderthumb', 125, 125, true ); //slider thumbnails
}

/*-----------------------------------------------------------------------------------*/
/*  Layout style class
/*-----------------------------------------------------------------------------------*/
function mts_layout_class($classes) {
    $mts_options = get_option('magxp');
    if (!is_single()) {
        if (empty($mts_options['mts_homepage_layout'])) {
            $layout = 'grid';
        } else {
            $layout = 'blog';
        }
        $classes[] = "mts-$layout-layout";
    }
    return $classes;
}
add_filter('body_class','mts_layout_class');

/*-----------------------------------------------------------------------------------*/
/*  Custom Menu Support
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'menus' );
if ( function_exists( 'register_nav_menus' ) ) {
    register_nav_menus(
        array(
          'primary-menu' => 'Primary Menu'
        )
    );
}

/*-----------------------------------------------------------------------------------*/
/*  Enable Widgetized sidebar and Footer
/*-----------------------------------------------------------------------------------*/
if ( function_exists('register_sidebar') ) {   
    function mts_register_sidebars() {
        $mts_options = get_option('magxp');
        
        // Default
        register_sidebar(array(
            'name' => 'Sidebar',
            'description'   => __( 'Default sidebar.', 'mythemeshop' ),
            'id' => 'sidebar',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // Top level footer widget areas
        if (!empty($mts_options['mts_top_footer'])) {
            if (empty($mts_options['mts_top_footer_num'])) $mts_options['mts_top_footer_num'] = 4;
            register_sidebars($mts_options['mts_top_footer_num'], array(
                'name' => __('Top Footer %d', 'mythemeshop'),
                'description'   => __( 'Appears at the top of the footer.', 'mythemeshop' ),
                'id' => 'footer-top',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));
        }
        // Bottom level footer widget areas
        if (!empty($mts_options['mts_bottom_footer'])) {
            if (empty($mts_options['mts_bottom_footer_num'])) $mts_options['mts_bottom_footer_num'] = 3;
            register_sidebars($mts_options['mts_bottom_footer_num'], array(
                'name' => __('Bottom Footer %d', 'mythemeshop'),
                'description'   => __( 'Appears at the bottom of the footer.', 'mythemeshop' ),
                'id' => 'footer-bottom',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));
        }
        
        // Custom sidebars
        if (!empty($mts_options['mts_custom_sidebars']) && is_array($mts_options['mts_custom_sidebars'])) {
            foreach($mts_options['mts_custom_sidebars'] as $sidebar) {
                if (!empty($sidebar['mts_custom_sidebar_id']) && !empty($sidebar['mts_custom_sidebar_id']) && $sidebar['mts_custom_sidebar_id'] != 'sidebar-') {
                    register_sidebar(array('name' => ''.$sidebar['mts_custom_sidebar_name'].'','id' => ''.sanitize_title(strtolower($sidebar['mts_custom_sidebar_id'])).'','before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));
                }
            }
        }

        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
            // Register WooCommerce Shop and Single Product Sidebar
            register_sidebar(array(
                'name' => 'Shop Page Sidebar',
                'description'   => __( 'Appears on Shop main page and product archive pages.', 'mythemeshop' ),
                'id' => 'shop-sidebar',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));
            register_sidebar(array(
                'name' => 'Single Product Sidebar',
                'description'   => __( 'Appears on single product pages.', 'mythemeshop' ),
                'id' => 'product-sidebar',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>',
            ));
        }
    }
    
    add_action('widgets_init', 'mts_register_sidebars');
}

function mts_custom_sidebar() {
    $mts_options = get_option('magxp');
    
    // Default sidebar
    $sidebar = 'Sidebar';

    if (is_home() && !empty($mts_options['mts_sidebar_for_home'])) $sidebar = $mts_options['mts_sidebar_for_home']; 
    if (is_single() && !empty($mts_options['mts_sidebar_for_post'])) $sidebar = $mts_options['mts_sidebar_for_post'];
    if (is_page() && !empty($mts_options['mts_sidebar_for_page'])) $sidebar = $mts_options['mts_sidebar_for_page'];
    
    // Archives
    if (is_archive() && !empty($mts_options['mts_sidebar_for_archive'])) $sidebar = $mts_options['mts_sidebar_for_archive'];
    if (is_category() && !empty($mts_options['mts_sidebar_for_category'])) $sidebar = $mts_options['mts_sidebar_for_category'];
    if (is_tag() && !empty($mts_options['mts_sidebar_for_tag'])) $sidebar = $mts_options['mts_sidebar_for_tag'];
    if (is_date() && !empty($mts_options['mts_sidebar_for_date'])) $sidebar = $mts_options['mts_sidebar_for_date'];
    if (is_author() && !empty($mts_options['mts_sidebar_for_author'])) $sidebar = $mts_options['mts_sidebar_for_author'];
    
    // Other
    if (is_search() && !empty($mts_options['mts_sidebar_for_search'])) $sidebar = $mts_options['mts_sidebar_for_search'];
    if (is_404() && !empty($mts_options['mts_sidebar_for_notfound'])) $sidebar = $mts_options['mts_sidebar_for_notfound'];

    // woo
    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
        if (is_shop() && !empty($mts_options['mts_sidebar_for_shop'])) $sidebar = $mts_options['mts_sidebar_for_shop'];
        if (is_product() && !empty($mts_options['mts_sidebar_for_product'])) $sidebar = $mts_options['mts_sidebar_for_product'];
        if (is_product_category() && !empty($mts_options['mts_sidebar_for_shop_category'])) $sidebar = $mts_options['mts_sidebar_for_shop_category'];
        if (is_product_tag() && !empty($mts_options['mts_sidebar_for_shop_tag'])) $sidebar = $mts_options['mts_sidebar_for_shop_tag'];
    }
    
    // Page/post specific custom sidebar
    if (is_page() || is_single() || (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && is_shop())) {
        wp_reset_postdata();
        global $post;
        $id = $post->ID;
        if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && is_shop()) {
            $id = woocommerce_get_page_id('shop');
        }
        $custom = get_post_meta($id,'_mts_custom_sidebar',true);
        if (!empty($custom)) $sidebar = $custom;
    }

    return $sidebar;
}

/*-----------------------------------------------------------------------------------*/
/*  Load Widgets & Shortcodes
/*-----------------------------------------------------------------------------------*/
// Add the 125x125 Ad Block Custom Widget
include("functions/widget-ad125.php");

// Add the 300x250 Ad Block Custom Widget
include("functions/widget-ad300.php");

// Add the Latest Tweets Custom Widget
include("functions/widget-tweets.php");

// Add Recent Posts Widget
include("functions/widget-recentposts.php");

// Add Related Posts Widget
include("functions/widget-relatedposts.php");

// Add Author Posts Widget
include("functions/widget-authorposts.php");

// Add Popular Posts Widget
include("functions/widget-popular.php");

// Add Facebook Like box Widget
include("functions/widget-fblikebox.php");

// Add Google Plus box Widget
include("functions/widget-googleplus.php");

// Add Subscribe Widget
include("functions/widget-subscribe.php");

// Add Social Profile Widget
include("functions/widget-social.php");

// Add Category Posts Widget
include("functions/widget-catposts.php");

// Add Category Posts Widget
include("functions/widget-postslider.php");

// Add Trending Posts Widget
include("functions/widget-trending.php");

// Add Top Posts Widget
include("functions/widget-topposts.php");

// Add Welcome message
include("functions/welcome-message.php");

// Theme Functions
include("functions/theme-actions.php");

// Plugin Activation
include("functions/class-tgm-plugin-activation.php");

/*-----------------------------------------------------------------------------------*/
/*  Filters customize wp_title
/*-----------------------------------------------------------------------------------*/
function mts_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() )
        return $title;

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'mythemeshop' ), max( $paged, $page ) );

    return $title;
}
add_filter( 'wp_title', 'mts_wp_title', 10, 2 );

/*-----------------------------------------------------------------------------------*/
/*  Javascsript
/*-----------------------------------------------------------------------------------*/
function mts_add_scripts() {
    $mts_options = get_option('magxp');

    wp_enqueue_script('jquery');

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
    
    wp_register_script('customscript', get_template_directory_uri() . '/js/customscript.js', true);
    wp_enqueue_script ('customscript');

        
    // Slider
    wp_register_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js');
    if((($mts_options['mts_featured_slider'] == '1' || $mts_options['mts_shop_slider'] == '1' || $mts_options['mts_gallery_slider'] == '1') && !is_singular()) || ($mts_options['mts_single_carousel'] == '1' && is_singular())) {
        wp_enqueue_script ('flexslider');
    }   

    global $is_IE;
    if ($is_IE) {
        wp_register_script ('html5shim', "http://html5shim.googlecode.com/svn/trunk/html5.js");
        wp_enqueue_script ('html5shim');
    }
    
}
add_action('wp_enqueue_scripts','mts_add_scripts');
   
function mts_load_footer_scripts() {  
    $mts_options = get_option('magxp');
    
    //Lightbox
    if($mts_options['mts_lightbox'] == '1') {
        wp_register_script('prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', true);
        wp_enqueue_script('prettyPhoto');
    }
    
    // Ajax Load More and Search Results
    wp_register_script('mts_ajax', get_template_directory_uri() . '/js/ajax.js', true);
    if(!empty($mts_options['mts_pagenavigation_type']) && $mts_options['mts_pagenavigation_type'] >= 2 && !is_singular()) {
        wp_enqueue_script('mts_ajax');
        
        wp_register_script('historyjs', get_template_directory_uri() . '/js/history.js', true);
        wp_enqueue_script('historyjs');
        
        // Add parameters for the JS
        global $wp_query;
        $max = $wp_query->max_num_pages;
        $paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
        $autoload = ($mts_options['mts_pagenavigation_type'] == 3);
        wp_localize_script(
            'mts_ajax',
            'mts_ajax_loadposts',
            array(
                'startPage' => $paged,
                'maxPages' => $max,
                'nextLink' => next_posts($max, false),
                'autoLoad' => $autoload,
                'i18n_loadmore' => __('Load More Posts', 'mythemeshop'),
                'i18n_nomore' => __('No more posts.', 'mythemeshop')
            )
        );
    }
    if(!empty($mts_options['mts_ajax_search'])) {
        wp_enqueue_script('mts_ajax');
        wp_localize_script(
            'mts_ajax',
            'mts_ajax_search',
            array(
                'url' => admin_url('admin-ajax.php'),
                'ajax_search' => '1'
            )
        );
        
    }
}  
add_action('wp_footer', 'mts_load_footer_scripts');  

if(!empty($mts_options['mts_ajax_search'])) {
    add_action('wp_ajax_mts_search', 'ajax_mts_search');
    add_action('wp_ajax_nopriv_mts_search', 'ajax_mts_search');
}

function mts_nojs_js_class() {
    echo '<script type="text/javascript">document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/,\'js\');</script>';
}
add_action('wp_head', 'mts_nojs_js_class');

/*-----------------------------------------------------------------------------------*/
/* Enqueue CSS
/*-----------------------------------------------------------------------------------*/
function mts_enqueue_css() {
    $mts_options = get_option('magxp');

    // Slider
    // register_style() taken out of IF() to be able to enqueue it from the slider widget
    wp_register_style('flexslider', get_template_directory_uri() . '/css/flexslider.css', 'style');
    if((($mts_options['mts_featured_slider'] == '1' || $mts_options['mts_shop_slider'] == '1' || $mts_options['mts_gallery_slider'] == '1') && !is_singular())
        || ($mts_options['mts_single_carousel'] == '1' && is_singular())) {
        wp_enqueue_style('flexslider');
    }

    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && (is_woocommerce() || is_cart() || is_checkout() || is_account_page()) ) {
        //WooCommerce
        wp_register_style('woocommerce', get_template_directory_uri() . '/css/woocommerce2.css', 'style');
        wp_enqueue_style('woocommerce');
    }
    
    //lightbox
    if($mts_options['mts_lightbox'] == '1') {
        wp_register_style('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', 'style');
        wp_enqueue_style('prettyPhoto');
    }
    
    //Font Awesome
    wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', 'style');
    wp_enqueue_style('fontawesome');
    
    wp_enqueue_style('stylesheet', get_stylesheet_directory_uri() . '/style.css', 'style');
    
    //Responsive
    if($mts_options['mts_responsive'] == '1') {
        wp_enqueue_style('responsive', get_template_directory_uri() . '/css/responsive.css', 'style');
    }
    
    $mts_header_bg = '';
    if ($mts_options['mts_header_bg_pattern_upload'] != '') {
        $mts_header_bg = $mts_options['mts_header_bg_pattern_upload'];
    } else {
        if($mts_options['mts_header_bg_pattern'] != '') {
            $mts_header_bg = get_template_directory_uri().'/images/'.$mts_options['mts_header_bg_pattern'].'.png';
        }
    }
    $mts_bg = '';
    if ($mts_options['mts_bg_pattern_upload'] != '') {
        $mts_bg = $mts_options['mts_bg_pattern_upload'];
    } else {
        if(!empty($mts_options['mts_bg_pattern'])) {
            $mts_bg = get_template_directory_uri().'/images/'.$mts_options['mts_bg_pattern'].'.png';
        }
    }
    $mts_top_footer_bg = '';
    if ($mts_options['mts_top_footer_bg_pattern_upload'] != '') {
        $mts_top_footer_bg = $mts_options['mts_top_footer_bg_pattern_upload'];
    } else {
        if($mts_options['mts_top_footer_bg_pattern'] != '') {
            $mts_top_footer_bg = get_template_directory_uri().'/images/'.$mts_options['mts_top_footer_bg_pattern'].'.png';
        }
    }
    $mts_bottom_footer_bg = '';
    if ($mts_options['mts_bottom_footer_bg_pattern_upload'] != '') {
        $mts_bottom_footer_bg = $mts_options['mts_bottom_footer_bg_pattern_upload'];
    } else {
        if($mts_options['mts_bottom_footer_bg_pattern'] != '') {
            $mts_bottom_footer_bg = get_template_directory_uri().'/images/'.$mts_options['mts_bottom_footer_bg_pattern'].'.png';
        }
    }
    $mts_sclayout = '';
    $mts_shareit_left = '';
    $mts_shareit_right = '';
    $mts_author = '';
    $mts_header_section = '';
    if (is_page() || is_single() || (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && is_shop())) {
        $id = get_the_ID();
        if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && is_shop()) {
            $id = woocommerce_get_page_id('shop');
        }
        $mts_sidebar_location = get_post_meta( $id, '_mts_sidebar_location', true );
    } else {
        $mts_sidebar_location = '';
    }
    if ($mts_sidebar_location != 'right' && ($mts_options['mts_layout'] == 'sclayout' || $mts_sidebar_location == 'left')) {
        $mts_sclayout = '.article, .woocommerce.archive .article, .single-product.woocommerce .article { float: right;}
        .sidebar.c-4-12 { float: left; padding-right: 0; }';
        if($mts_options['mts_social_button_position'] == '3') {
            $mts_shareit_right = '.shareit { margin: 0 620px 0; border-left: 0; } .ss-full-width .shareit { margin: 0 950px 0 }';
        }
    }
    if ($mts_options['mts_show_logo'] == '0') {
        $mts_header_section = '.logo-wrap, .widget-header { display: none; }
        .secondary-navigation { margin-left: 0; }
        #header { min-height: 47px; }';
    }
    if($mts_options['mts_social_button_position'] == '3') {
        $mts_shareit_left = '.shareit { top: 327px; left: auto; z-index: 0; margin: 0 0 0 -130px; width: 90px; position: fixed; padding: 5px; background: #fff; -moz-box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.1); box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.1); }
        .share-item {margin: 2px;}';
    } else {
        $mts_shareit_left = '.shareit { margin-right: 0; margin-left: 0; }';
    }
    if($mts_options['mts_author_comment'] == '1') {
        $mts_author = '.commentlist .bypostauthor, .commentlist .children .bypostauthor {padding: 3%; background: #F1F1F1; width: 94%; }
        .bypostauthor:after { content: "Author"; position: absolute; right: 0; top: 14px; font-size: 12px; line-height: 2.4; padding: 0 15px 0 30px; }
        .bypostauthor:before { content: "\f044"; font-family: FontAwesome; position: absolute; right: 60px; font-size: 16px; z-index: 1; top: 16px; }';}
    $custom_css = "
        body {background-color:{$mts_options['mts_bg_color']}; }
        body {background-image: url({$mts_bg});}
        .main-header, #header ul.sub-menu li {background-color:{$mts_options['mts_header_bg_color']}; background-image: url({$mts_header_bg});}
        footer {background-color:{$mts_options['mts_top_footer_bg_color']}; background-image: url({$mts_top_footer_bg});}
        .bottom-footer-widgets {background-color:{$mts_options['mts_bottom_footer_bg_color']}; background-image: url({$mts_bottom_footer_bg});}
        .copyrights {background-color:{$mts_options['mts_copyrights_bg_color']}; }
        .single-carousel h6 a:hover, .f-widget ul.flex-direction-nav a, #navigation ul li a:hover, a:hover, .related-posts a:hover, .reply a, .title a:hover, .post-info a:hover, #tabber .inside li a:hover, .readMore a:hover, .fn a, .banner-content h2 a:hover, .fa-search:hover, .sidebar.c-4-12 a:hover, footer .f-widget a:hover, .star-rating-thumbs .icon-star:hover, .main-container .single_post-img header .post-info a:hover, #copyright-note a:hover, .banner-content .post-info a:hover, .main-container .single_post-img header .single-title a:hover, .search-top .ajax-search-results-container a:hover, .sidebar.c-4-12 .textwidget a, footer .f-widget .textwidget a, a, .widget .wpt_widget_content .tab_title.selected a, .widget .wp_review_tab_widget_content .tab_title.selected a, .comment-author .comment-reply-link, .woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before { color:{$mts_options['mts_color_scheme']}; }
            .footer-widgets .f-widget #searchform .fa-search, .f-widget .social-profile-icons ul li a:hover, .widget .bar, .latestPost .categories, .currenttext, .pagination a:hover, #header .fa-search:hover, #header .fa-search.active, .social a:hover, #searchform .fa-search, #move-to-top:hover, #copyright-note .toplink:hover, nav a#pull, #commentform input#submit, #commentform input#submit:hover, .mts-subscribe input[type='submit'], #move-to-top:hover, #tabber ul.tabs li a.selected, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce nav.woocommerce-pagination ul li a, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce-page nav.woocommerce-pagination ul li span.current, .woocommerce #content nav.woocommerce-pagination ul li span.current, .woocommerce-page #content nav.woocommerce-pagination ul li span.current, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce-page nav.woocommerce-pagination ul li a:hover, .woocommerce #content nav.woocommerce-pagination ul li a:hover, .woocommerce-page #content nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce-page nav.woocommerce-pagination ul li a:focus, .woocommerce #content nav.woocommerce-pagination ul li a:focus, .woocommerce-page #content nav.woocommerce-pagination ul li a:focus, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .tagcloud a, thead, #load-posts a:hover, .widget .wpt_widget_content .tab_title.selected a:before, .widget .wp_review_tab_widget_content .tab_title.selected a:before, .widget .wpt_widget_content #tags-tab-content ul li a, .single .pagination a:hover, .pace .pace-progress, #mobile-menu-wrapper ul li a:hover, #searchform #searchsubmit, #load-posts a { background-color:{$mts_options['mts_color_scheme']}; color: #fff!important; }
        .flex-control-thumbs .flex-active{ border-top:3px solid {$mts_options['mts_color_scheme']};}
        #navigation ul li a, #header ul.sub-menu, .search-top .hideinput, #featured-thumbnail, .search-top #s, .search-top .ajax-search-results-container, #author:focus, #email:focus, #url:focus, #comment:focus { border-color: {$mts_options['mts_color_scheme']}; }
        .sidebar .widget h3 { background: {$mts_options['mts_sidebar_title_bg_color']}; }
        {$mts_sclayout}
        {$mts_shareit_left}
        {$mts_shareit_right}
        {$mts_author}
        {$mts_header_section}
        {$mts_options['mts_custom_css']}
            ";
    wp_add_inline_style( 'stylesheet', $custom_css );
}
add_action('wp_enqueue_scripts', 'mts_enqueue_css', 99);

/*-----------------------------------------------------------------------------------*/
/*  Filters that allow shortcodes in Text Widgets
/*-----------------------------------------------------------------------------------*/
add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');
add_filter('the_content_rss', 'do_shortcode');

/*-----------------------------------------------------------------------------------*/
/*  Custom Comments template
/*-----------------------------------------------------------------------------------*/
function mts_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div id="comment-<?php comment_ID(); ?>" style="position:relative;">
            <div class="comment-author vcard">
                <?php echo get_avatar( $comment->comment_author_email, 65 ); ?>
                <div class="author-details">
                    <?php printf(__('<span class="fn">%s</span>', 'mythemeshop'), get_comment_author_link()) ?> 
                    <?php $mts_options = get_option('magxp'); if($mts_options['mts_comment_date'] == '1') { ?>
                        <span class="ago"><?php comment_date(get_option( 'date_format' )); ?></span>
                    <?php } ?>
                    <span class="comment-meta">
                        <?php edit_comment_link(__('(Edit)', 'mythemeshop'),'  ','') ?>
                    </span>
                </div>
                <?php if ($comment->comment_approved == '0') : ?>
                    <em><?php _e('Your comment is awaiting moderation.', 'mythemeshop') ?></em>
                    <br />
                <?php endif; ?>
                <div class="commentmetadata">
                    <?php comment_text() ?>
                    <div class="reply">
                        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    </div>
                </div>
            </div>
        </div>
    </li>
<?php }

/*-----------------------------------------------------------------------------------*/
/*  excerpt
/*-----------------------------------------------------------------------------------*/
function mts_excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt);
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

/*-----------------------------------------------------------------------------------*/
/*  Remove more link from the_content and use custom read more
/*-----------------------------------------------------------------------------------*/
add_filter( 'the_content_more_link', 'mts_remove_more_link', 10, 2 );
function mts_remove_more_link( $more_link, $more_link_text ) {
    return '';
}
// shorthand function to check for more tag in post
function mts_post_has_moretag() {
    global $post;
    return strpos($post->post_content, '<!--more-->');
}

/*-----------------------------------------------------------------------------------*/
/* nofollow to next/previous links
/*-----------------------------------------------------------------------------------*/
function mts_pagination_add_nofollow($content) {
    return 'rel="nofollow"';
}
add_filter('next_posts_link_attributes', 'mts_pagination_add_nofollow' );
add_filter('previous_posts_link_attributes', 'mts_pagination_add_nofollow' );

/*-----------------------------------------------------------------------------------*/
/* Nofollow to category links
/*-----------------------------------------------------------------------------------*/
add_filter( 'the_category', 'mts_add_nofollow_cat' ); 
function mts_add_nofollow_cat( $text ) {
$text = str_replace('rel="category tag"', 'rel="nofollow"', $text); return $text;
}

/*-----------------------------------------------------------------------------------*/ 
/* nofollow post author link
/*-----------------------------------------------------------------------------------*/
add_filter('the_author_posts_link', 'mts_nofollow_the_author_posts_link');
function mts_nofollow_the_author_posts_link ($link) {
return str_replace('<a href=', '<a rel="nofollow" href=',$link); 
}

/*-----------------------------------------------------------------------------------*/ 
/* nofollow to reply links
/*-----------------------------------------------------------------------------------*/
function mts_add_nofollow_to_reply_link( $link ) {
return str_replace( '")\'>', '")\' rel=\'nofollow\'>', $link );
}
add_filter( 'comment_reply_link', 'mts_add_nofollow_to_reply_link' );

/*-----------------------------------------------------------------------------------*/
/* removes the WordPress version from your header for security
/*-----------------------------------------------------------------------------------*/
function wb_remove_version() {
    return '<!--Theme by MyThemeShop.com-->';
}
add_filter('the_generator', 'wb_remove_version');
    
/*-----------------------------------------------------------------------------------*/
/* Removes Trackbacks from the comment count
/*-----------------------------------------------------------------------------------*/
add_filter('get_comments_number', 'mts_comment_count', 0);
function mts_comment_count( $count ) {
    if ( ! is_admin() ) {
        global $id;
        $comments = get_comments('status=approve&post_id=' . $id);
        $comments_by_type = separate_comments($comments);
        return count($comments_by_type['comment']);
    } else {
        return $count;
    }
}

/*-----------------------------------------------------------------------------------*/
/* adds a class to the post if there is a thumbnail
/*-----------------------------------------------------------------------------------*/
function has_thumb_class($classes) {
    global $post;
    if( has_post_thumbnail($post->ID) ) { $classes[] = 'has_thumb'; }
        return $classes;
}
add_filter('post_class', 'has_thumb_class');

/*-----------------------------------------------------------------------------------*/ 
/* Breadcrumb
/*-----------------------------------------------------------------------------------*/
function mts_the_breadcrumb() {
    echo '<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="';
    echo home_url();
    echo '" rel="nofollow">'.__('Home','mythemeshop');
    echo "<span class=\"arrow\"></span></a></span>";
    if (is_category() || is_single()) {
        echo "<span typeof=\"v:Breadcrumb\" property=\"v:title\">";
        the_category(' &middot; ');
        echo "<span class=\"arrow\"></span>";
        echo "</span>";
    } elseif (is_page()) {
        echo "<span typeof=\"v:Breadcrumb\" property=\"v:title\">";
        echo the_title();
        echo "<span class=\"arrow\"></span>";
        echo "</span>";
    } elseif (is_search()) {
        echo "<span typeof=\"v:Breadcrumb\" property=\"v:title\">";
        echo __('Search Results for','mythemeshop')." ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
        echo "<span class=\"arrow\"></span>";
        echo "</span>";
    }
}

/*-----------------------------------------------------------------------------------*/ 
/* Pagination
/*-----------------------------------------------------------------------------------*/
function mts_pagination($pages = '', $range = 3) { 
    $showitems = ($range * 3)+1;
    global $paged; if(empty($paged)) $paged = 1;
    if($pages == '') {
        global $wp_query; $pages = $wp_query->max_num_pages; 
        if(!$pages){ $pages = 1; } 
    }
    if(1 != $pages) { 
        echo "<div class='pagination'><ul>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) 
            echo "<li><a rel='nofollow' href='".get_pagenum_link(1)."'><i class='fa fa-angle-double-left'></i></a></li>";
        if($paged > 1 && $showitems < $pages) 
            echo "<li><a rel='nofollow' href='".get_pagenum_link($paged - 1)."' class='inactive'><i class='fa fa-angle-left'></i></a></li>";
        for ($i=1; $i <= $pages; $i++){ 
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) { 
                echo ($paged == $i)? "<li class='current'><span class='currenttext'>".$i."</span></li>":"<li><a rel='nofollow' href='".get_pagenum_link($i)."' class='inactive'>".$i."</a></li>";
            } 
        } 
        if ($paged < $pages && $showitems < $pages) 
            echo "<li><a rel='nofollow' href='".get_pagenum_link($paged + 1)."' class='inactive'><i class='fa fa-angle-right'></i></a></li>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) 
            echo "<li><a rel='nofollow' class='inactive' href='".get_pagenum_link($pages)."'><i class='fa fa-angle-double-right'></i></a></li>";
            echo "</ul></div>"; 
    }
}

/*-----------------------------------------------------------------------------------*/ 
/* AJAX Search results
/*-----------------------------------------------------------------------------------*/
function ajax_mts_search() {
    $query = $_REQUEST['q'];//esc_html($_REQUEST['q']);
    // No need to esc as WP_Query escapes data before performing the database query
    $search_query = new WP_Query(array('s' => $query, 'posts_per_page' => 3));
    $search_count = new WP_Query(array('s' => $query, 'posts_per_page' => -1));
    $search_count = $search_count->post_count;
    if (!empty($query) && $search_query->have_posts()) : 
        //echo '<h5>Results for: '. $query.'</h5>';
        echo '<ul class="ajax-search-results">';
        while ($search_query->have_posts()) : $search_query->the_post();
            ?><li>
                <a href="<?php the_permalink(); ?>">
                    <?php if(has_post_thumbnail()): ?>
                        <?php the_post_thumbnail('widgetthumb',array('title' => '')); ?>
                    <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/smallthumb.png" alt="<?php the_title(); ?>" class="wp-post-image" />
                    <?php endif; ?>
                    
                    <?php the_title(); ?>   
                </a>
                <div class="meta">
                        <span class="thetime"><?php the_time('F j, Y'); ?></span>
                </div> <!-- / .meta -->

            </li>   
            <?php
        endwhile;
        echo '</ul>';
        echo '<div class="ajax-search-meta"><span class="results-count">'.$search_count.' '.__('Results', 'mythemeshop').'</span><a href="'.get_search_link($query).'" class="results-link">Show all results</a></div>';
    else:
        echo '<div class="no-results">'.__('No results found.', 'mythemeshop').'</div>';
    endif;
        
    exit; // required for AJAX in WP
}
/*-----------------------------------------------------------------------------------*/
/* Redirect feed to feedburner
/*-----------------------------------------------------------------------------------*/

if ( $mts_options['mts_feedburner'] != '') {
function mts_rss_feed_redirect() {
    $mts_options = get_option('magxp');
    global $feed;
    $new_feed = $mts_options['mts_feedburner'];
    if (!is_feed()) {
            return;
    }
    if (preg_match('/feedburner/i', $_SERVER['HTTP_USER_AGENT'])){
            return;
    }
    if ($feed != 'comments-rss2') {
            if (function_exists('status_header')) status_header( 302 );
            header("Location:" . $new_feed);
            header("HTTP/1.1 302 Temporary Redirect");
            exit();
    }
}
add_action('template_redirect', 'mts_rss_feed_redirect');
}

/*-----------------------------------------------------------------------------------*/
/* Single Post Pagination
/*-----------------------------------------------------------------------------------*/
function mts_wp_link_pages_args_prevnext_add($args)
{
    global $page, $numpages, $more, $pagenow;
    if (!$args['next_or_number'] == 'next_and_number')
        return $args; 
    $args['next_or_number'] = 'number'; 
    if (!$more)
        return $args; 
    if($page-1) 
        $args['before'] .= _wp_link_page($page-1)
        . $args['link_before']. $args['previouspagelink'] . $args['link_after'] . '</a>'
    ;
    if ($page<$numpages) 
    
        $args['after'] = _wp_link_page($page+1)
        . $args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a>'
        . $args['after']
    ;
    return $args;
}
add_filter('wp_link_pages_args', 'mts_wp_link_pages_args_prevnext_add');

/*-----------------------------------------------------------------------------------*/
/* WooCommerce
/*-----------------------------------------------------------------------------------*/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
add_theme_support('woocommerce');

// Image Fliper
include("woocommerce/image-flipper.php");

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3; // 3 products per row
    }
}

// Redefine woocommerce_output_related_products()
if ( ! function_exists( 'woocommerce_output_related_products' ) ) {

    /**
     * Output the related products.
     *
     * @access public
     * @subpackage  Product
     * @return void
     */
    function woocommerce_output_related_products() {

        $args = array(
            'posts_per_page' => 3,
            'columns' => 1,
            'orderby' => 'rand'
        );

        woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
    }
}
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
/*** Hook in on activation */
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'mythemeshop_woocommerce_image_dimensions', 1 );
 
/*** Define image sizes */
function mythemeshop_woocommerce_image_dimensions() {
    $catalog = array(
        'width'     => '307',   // px
        'height'    => '227',   // px
        'crop'      => 1        // true
    );
    $single = array(
        'width'     => '440',   // px
        'height'    => '600',   // px
        'crop'      => 1        // true
    );
    $thumbnail = array(
        'width'     => '67',    // px
        'height'    => '45',    // px
        'crop'      => 1        // false
    ); 
    // Image sizes
    update_option( 'shop_catalog_image_size', $catalog );       // Product category thumbs
    update_option( 'shop_single_image_size', $single );         // Single product image
    update_option( 'shop_thumbnail_image_size', $thumbnail );   // Image gallery thumbs
}

add_filter ( 'woocommerce_product_thumbnails_columns', 'mts_thumb_cols' );
    function mts_thumb_cols() {
        return 4; // .last class applied to every 4th thumbnail
    }
}

// Display 24 products per page. Goes in functions.php
$mts_home_producst = $mts_options['mts_shop_products'];
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '.$mts_home_producst.';' ), 20 );

/*------------[ Cart ]-------------*/
if ( ! function_exists( 'mts_cart' ) ) {
    function mts_cart() { 
        global $mts_options;
        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
            <div class="mts-cart">
                <?php global $woocommerce; ?>
                <span>
                    <i class="fa fa-user"></i> 
                    <?php if ( is_user_logged_in() ) { ?>
                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','mythemeshop'); ?>"><?php _e('My Account','mythemeshop'); ?></a>
                    <?php } 
                    else { ?>
                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login','mythemeshop'); ?>"><?php _e('Login ','mythemeshop'); ?></a>
                    <?php } ?>
                </span>
                <span>
                    <i class="fa fa-shopping-cart"></i> <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'mythemeshop'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'mythemeshop'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
                </span>
            </div>
        <?php } 
    }

    // Ensure cart contents update when products are added to the cart via AJAX
    add_filter('add_to_cart_fragments', 'mts_header_add_to_cart_fragment');
     
    function mts_header_add_to_cart_fragment( $fragments ) {
        global $woocommerce;
        ob_start(); ?>
        
        <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'mythemeshop'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'mythemeshop'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
        
        <?php $fragments['a.cart-contents'] = ob_get_clean();
        return $fragments;
    }
}

/*-----------------------------------------------------------------------------------*/
/* add <!-- next-page --> button to tinymce
/*-----------------------------------------------------------------------------------*/
add_filter('mce_buttons','wysiwyg_editor');
function wysiwyg_editor($mce_buttons) {
   $pos = array_search('wp_more',$mce_buttons,true);
   if ($pos !== false) {
       $tmp_buttons = array_slice($mce_buttons, 0, $pos+1);
       $tmp_buttons[] = 'wp_page';
       $mce_buttons = array_merge($tmp_buttons, array_slice($mce_buttons, $pos+1));
   }
   return $mce_buttons;
}

/*-----------------------------------------------------------------------------------*/
/*  Custom Gravatar Support
/*-----------------------------------------------------------------------------------*/
if( !function_exists( 'mts_custom_gravatar' ) ) {
    function mts_custom_gravatar( $avatar_defaults ) {
        $mts_avatar = get_template_directory_uri() . '/images/gravatar.png';
        $avatar_defaults[$mts_avatar] = 'Custom Gravatar (/images/gravatar.png)';
        return $avatar_defaults;
    }
    add_filter( 'avatar_defaults', 'mts_custom_gravatar' );
}

/*-----------------------------------------------------------------------------------*/
/*  Sidebar Selection meta box
/*-----------------------------------------------------------------------------------*/
function mts_add_sidebar_metabox() {
    $screens = array('post', 'page');
    if (post_type_exists('product')) {
        $screens[] = 'product';
    }
    foreach ($screens as $screen) {
        add_meta_box(
            'mts_sidebar_metabox',           // id
            __('Sidebar', 'mythemeshop'),    // title
            'mts_inner_sidebar_metabox',     // callback
            $screen,                         // post_type
            'side',                          // context (normal, advanced, side)
            'high'                           // priority (high, core, default, low)
        );
    }
}
add_action('add_meta_boxes', 'mts_add_sidebar_metabox');


/**
 * Print the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function mts_inner_sidebar_metabox($post) {
    global $wp_registered_sidebars;
    
    // Add an nonce field so we can check for it later.
    wp_nonce_field('mts_inner_sidebar_metabox', 'mts_inner_sidebar_metabox_nonce');
    
    /*
    * Use get_post_meta() to retrieve an existing value
    * from the database and use the value for the form.
    */
    $custom_sidebar = get_post_meta( $post->ID, '_mts_custom_sidebar', true );
    $sidebar_location = get_post_meta( $post->ID, '_mts_sidebar_location', true );

    // Select custom sidebar from dropdown
    echo '<select name="mts_custom_sidebar" style="margin-bottom: 10px;">';
    echo '<option value="" '.selected('', $custom_sidebar).'>'.__('Default', 'mythemeshop').'</option>';
    
    // Exclude built-in sidebars
    $hidden_sidebars = array('sidebar', 'footer-top', 'footer-top-2', 'footer-top-3', 'footer-top-4', 'footer-bottom', 'footer-bottom-2', 'footer-bottom-3', 'footer-bottom-4', 'product-sidebar', 'shop-sidebar');    
    
    foreach ($wp_registered_sidebars as $sidebar) {
        if (!in_array($sidebar['id'], $hidden_sidebars)) {
            echo '<option value="'.esc_attr($sidebar['id']).'" '.selected($sidebar['id'], $custom_sidebar, false).'>'.$sidebar['name'].'</option>';
        }
    }
    echo '<option value="mts_nosidebar" '.selected('mts_nosidebar', $custom_sidebar).'>'.__('No sidebar (full width content)', 'mythemeshop').'</option>';
    echo '</select><br />';
    
    // Select single layout (left/right sidebar)
    echo '<label for="mts_sidebar_location_default" style="display: inline-block; margin-right: 20px;"><input type="radio" name="mts_sidebar_location" id="mts_sidebar_location_default" value=""'.checked('', $sidebar_location, false).'>'.__('Default Side', 'mythemeshop').'</label>';
    echo '<label for="mts_sidebar_location_left" style="display: inline-block; margin-right: 20px;"><input type="radio" name="mts_sidebar_location" id="mts_sidebar_location_left" value="left"'.checked('left', $sidebar_location, false).'>'.__('Left', 'mythemeshop').'</label>';
    echo '<label for="mts_sidebar_location_right" style="display: inline-block; margin-right: 20px;"><input type="radio" name="mts_sidebar_location" id="mts_sidebar_location_right" value="right"'.checked('right', $sidebar_location, false).'>'.__('Right', 'mythemeshop').'</label>';
     
    //debug
    global $wp_meta_boxes;
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function mts_save_custom_sidebar( $post_id ) {
    
    /*
    * We need to verify this came from our screen and with proper authorization,
    * because save_post can be triggered at other times.
    */
    
    // Check if our nonce is set.
    if ( ! isset( $_POST['mts_inner_sidebar_metabox_nonce'] ) )
    return $post_id;
    
    $nonce = $_POST['mts_inner_sidebar_metabox_nonce'];
    
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $nonce, 'mts_inner_sidebar_metabox' ) )
      return $post_id;
    
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return $post_id;
    
    // Check the user's permissions.
    if ( 'page' == $_POST['post_type'] ) {
    
    if ( ! current_user_can( 'edit_page', $post_id ) )
        return $post_id;
    
    } else {
    
    if ( ! current_user_can( 'edit_post', $post_id ) )
        return $post_id;
    }
    
    /* OK, its safe for us to save the data now. */
    
    // Sanitize user input.
    $sidebar_name = sanitize_text_field( $_POST['mts_custom_sidebar'] );
    $sidebar_location = sanitize_text_field( $_POST['mts_sidebar_location'] );
    
    // Update the meta field in the database.
    update_post_meta( $post_id, '_mts_custom_sidebar', $sidebar_name );
    update_post_meta( $post_id, '_mts_sidebar_location', $sidebar_location );
}
add_action( 'save_post', 'mts_save_custom_sidebar' );

/*-----------------------------------------------------------------------------------*/
/*  Post Template Selection meta box
/*-----------------------------------------------------------------------------------*/
function mts_add_posttemplate_metabox() {
    add_meta_box(
        'mts_posttemplate_metabox',         // id
        __('Template', 'mythemeshop'),      // title
        'mts_inner_posttemplate_metabox',   // callback
        'post',                             // post_type
        'side',                             // context (normal, advanced, side)
        'high'                              // priority (high, core, default, low)
    );
}
add_action('add_meta_boxes', 'mts_add_posttemplate_metabox');


/**
 * Print the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function mts_inner_posttemplate_metabox($post) {
    global $wp_registered_sidebars;
    
    // Add an nonce field so we can check for it later.
    wp_nonce_field('mts_inner_posttemplate_metabox', 'mts_inner_posttemplate_metabox_nonce');
    
    /*
    * Use get_post_meta() to retrieve an existing value
    * from the database and use the value for the form.
    */
    $posttemplate = get_post_meta( $post->ID, '_mts_posttemplate', true );

    // Select custom sidebar from dropdown
    echo '<select name="mts_posttemplate" style="margin-bottom: 10px;">';
    echo '<option value="0" '.selected('0', $posttemplate).'>Post Template 1</option>';
    echo '<option value="1" '.selected('1', $posttemplate).'>Post Template 2</option>';
    echo '</select><br />';
    
    //debug
    //global $wp_meta_boxes;
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function mts_save_posttemplate( $post_id ) {
    
    /*
    * We need to verify this came from our screen and with proper authorization,
    * because save_post can be triggered at other times.
    */
    
    // Check if our nonce is set.
    if ( ! isset( $_POST['mts_inner_posttemplate_metabox_nonce'] ) )
    return $post_id;
    
    $nonce = $_POST['mts_inner_posttemplate_metabox_nonce'];
    
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $nonce, 'mts_inner_posttemplate_metabox' ) )
      return $post_id;
    
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return $post_id;
    
    // Check the user's permissions.
    if ( 'page' == $_POST['post_type'] ) {
    
    if ( ! current_user_can( 'edit_page', $post_id ) )
        return $post_id;
    
    } else {
    
    if ( ! current_user_can( 'edit_post', $post_id ) )
        return $post_id;
    }
    
    /* OK, its safe for us to save the data now. */
    
    // Sanitize user input.
    $posttemplate = sanitize_text_field( $_POST['mts_posttemplate'] );
    
    // Update the meta field in the database.
    update_post_meta( $post_id, '_mts_posttemplate', $posttemplate );
}
add_action( 'save_post', 'mts_save_posttemplate' );

function mts_get_posttemplate($single_template) {
     global $post;
     $posttemplate = get_post_meta( $post->ID, '_mts_posttemplate', true );
     if ($posttemplate == '1') {
          $single_template = dirname( __FILE__ ) . '/single2.php';
     }
     return $single_template;
}
add_filter( 'single_template', 'mts_get_posttemplate' );

/*
 * Function similar to wp_link_pages but outputs an ordered list instead and adds a class of current to the current page
 */
 
function mts_link_pages( $args = '' ) {
    $defaults = array(
        'before'           => '<p>' . __( 'Pages:' ), 'after' => '</p>',
        'link_before'      => '', 'link_after' => '',
        'next_or_number'   => 'number', 'nextpagelink' => __( 'Next page' ),
        'previouspagelink' => __( 'Previous page' ), 'pagelink' => '%',
        'echo'             => 1
    );
 
    $r = wp_parse_args( $args, $defaults );
    $r = apply_filters( 'wp_link_pages_args', $r );
    extract( $r, EXTR_SKIP );
 
    global $page, $numpages, $multipage, $more, $pagenow;
 
    $output = '';
    if ( $multipage ) {
        if ( 'number' == $next_or_number ) {
            $output .= $before;
            $output .= '<ul>';
            for ( $i = 1; $i < ( $numpages + 1 ); $i = $i + 1 ) {
                $j = str_replace( '%', $i, $pagelink );
                if ( ( $i == $page )) {
                    $output .= '<li class="current"><span class="currenttext">';
                } else {
                    $output .= '</span><li>';
                }
                if ( ( $i != $page ) || ( ( ! $more ) && ( $page == 1 ) ) ) {
                    $output .= _wp_link_page( $i );
                }
                $output .= $link_before . $j . $link_after;
                if ( ( $i != $page ) || ( ( ! $more ) && ( $page == 1 ) ) )
                    $output .= '</a>';
            }
            $output .= '</li>';
            $output .= $after;
        } else {
            if ( $more ) {
                $output .= $before;
                $i = $page - 1;
                if ( $i && $more ) {
                    $output .= _wp_link_page( $i );
                    $output .= $link_before . $previouspagelink . $link_after . '</a>';
                }
                $i = $page + 1;
                if ( $i <= $numpages && $more ) {
                    $output .= _wp_link_page( $i );
                    $output .= $link_before . $nextpagelink . $link_after . '</a>';
                }
                $output .= '</ul>';
                $output .= $after;
            }
        }
    }
 
    if ( $echo )
        echo $output;
 
    return $output;
}

// Color custom field for menu items
include('functions/edit-menu-colors.php');

// Category Colors
// 1. restructure array for easier handling
function mts_category_colors_array() {
    $mts_options = get_option('magxp');
    $return = array();
    if (!empty($mts_options['mts_category_colors'])) {
        foreach ($mts_options['mts_category_colors'] as $cc) {
            $return[$cc['mts_cc_category']] = $cc['mts_cc_color'];
        }
    }
    return $return;
}
// 2. get category color for given post
function mts_get_category_color($cat_id = null) {
    $cat_id = intval($cat_id);
    if (empty($cat_id)) {
        $category = get_the_category();
        // prevent error if no category
        if (empty($category)) return false;
        $cat_id = $category[0]->term_id;
    }
    
    $colors = mts_category_colors_array();
    $return = false;
    if (!empty($colors[$cat_id])) {
        $return = $colors[$cat_id];
    }
    return $return;
}

function mts_truncate($str, $length = 40) {
    if (mb_strlen($str) > $length) {
        return mb_substr($str, 0, $length).'...';
    } else {
        return $str;
    }
}

// Colorize WP Review total using filter
function mts_color_review_total($content, $id, $type, $total) {
    if ($type == 'star') {
        $color = mts_get_category_color();
        if(empty($color)) {
            $mts_options = get_option('magxp');
            $color = $mts_options['mts_color_scheme'];
        }
        $content = preg_replace('/"review-type-[^"]+"/', '$0 style="color: '.$color.';"', $content);
    } else {
        $color = mts_get_category_color();
        if(empty($color)) {
            $mts_options = get_option('magxp');
            $color = $mts_options['mts_color_scheme'];
        }
        $content = preg_replace('/"review-type-[^"]+"/', '$0 style="color:#fff;background-color: '.$color.';"', $content);
    }
    return $content;
}
add_filter('wp_review_show_total', 'mts_color_review_total', 10, 4);

// Set default colors for new reviews
function new_default_review_colors($colors) {
    $colors = array(
        'color' => '#FFCA00',
        'fontcolor' => '#fff',
        'bgcolor1' => '#151515',
        'bgcolor2' => '#151515',
        'bordercolor' => '#151515'
    );
  return $colors;
}
add_filter( 'wp_review_default_colors', 'new_default_review_colors' );
 
// Set default location for new reviews
function new_default_review_location($position) {
  $position = 'top';
  return $position;
}
add_filter( 'wp_review_default_location', 'new_default_review_location' );


/*-----------------------------------------------------------------------------------*/
/*  Required theme plugins activation
/*-----------------------------------------------------------------------------------*/
add_action( 'tgmpa_register', 'mts_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 */
function mts_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        array(
            'name'      => 'WP Review ',
            'slug'      => 'wp-review',
            'required'  => false,
        ),

        array(
            'name'      => 'WP Shortcode ',
            'slug'      => 'wp-shortcode',
            'required'  => false,
        ),

        array(
            'name'      => 'WP Tab Widget ',
            'slug'      => 'wp-tab-widget',
            'required'  => false,
        ),

        array(
            'name'      => 'MyThemeShop Connect ',
            'slug'      => 'mythemeshop-connect',
            'required'  => false,
        ),

    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'id'           => 'mts-plugins',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'mts-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'mythemeshop' ),
            'menu_title'                      => __( 'Install Plugins', 'mythemeshop' ),
            'installing'                      => __( 'Installing Plugin: %s', 'mythemeshop' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'mythemeshop' ),
            //'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'mythemeshop' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'mythemeshop' ), // %1$s = plugin name(s).
            //'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'mythemeshop' ), // %1$s = plugin name(s).
            //'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'mythemeshop' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'mythemeshop' ), // %1$s = plugin name(s).
            //'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'mythemeshop' ), // %1$s = plugin name(s).
            //'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'mythemeshop' ), // %1$s = plugin name(s).
            //'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'mythemeshop' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'mythemeshop' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'mythemeshop' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'mythemeshop' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'mythemeshop' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'mythemeshop' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}

/* WP Mega Menu */
function megamenu_parent_element( $selector ) {
    return '#header';
}
add_filter( 'wpmm_container_selector', 'megamenu_parent_element' );

function menu_item_color( $item_output, $item_color, $item, $depth, $args ) {
    if (!empty($item_color))
        return $item_output.'<style>#menu-item-'. $item->ID . ' a, .menu-item-'. $item->ID . '-megamenu, #menu-item-'. $item->ID . ' .sub-menu { border-color: ' . $item_color . ' !important; } 
#menu-item-'. $item->ID . ' a:hover, #wpmm-megamenu.menu-item-'. $item->ID . '-megamenu a:hover, #wpmm-megamenu.menu-item-'. $item->ID . '-megamenu .wpmm-posts .wpmm-entry-title a:hover { color: '.$item_color.' !important; }</style>';
    else
        return $item_output;
}
add_filter( 'wpmm_color_output', 'menu_item_color', 10, 5 );

?>