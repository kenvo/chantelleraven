<?php get_header(); ?>
<?php $mts_options = get_option('magxp'); ?>
<?php $article_class = 'article';
    if (mts_custom_sidebar() == 'mts_nosidebar') {
        $article_class = 'ss-full-width';
    } ?>
<div class="main-container">
	<div class="single_post-img"> 
		<div class="slides-shadow"></div>
		<?php the_post_thumbnail('slider'); ?>
		<header class="clearfix">
			<h1 class="title single-title"><?php the_title(); ?></h1>
			<div class="post-info">
				<?php if(isset($mts_options['mts_single_headline_meta_info']['author']) == '1') { ?>
					<span class="theauthor"><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></span>
				<?php } ?>
				<?php if(isset($mts_options['mts_single_headline_meta_info']['date']) == '1') { ?>
					<span class="thetime updated"><i class="fa fa-calendar"></i> <?php the_time( get_option( 'date_format' ) ); ?></span>
				<?php } ?>
				<?php if(isset($mts_options['mts_single_headline_meta_info']['category']) == '1') { ?>
					<span class="thecategory"><i class="fa fa-tags"></i> <?php the_category(', ') ?></span>
				<?php } ?>
				<?php if(isset($mts_options['mts_single_headline_meta_info']['comment']) == '1') { ?>
					<span class="thecomment"><i class="fa fa-comments"></i> <a rel="nofollow" href="<?php comments_link(); ?>"><?php echo comments_number();?></a></span>
				<?php } ?>
			</div>
		</header>
	</div>
	<div id="page" class="single clearfix">
		<article class="<?php echo $article_class; ?>">
			<div id="content_box" >
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class('g post'); ?>>
						<div class="single_post">
							<div class="post-single-content box mark-links">
								<?php if ($mts_options['mts_posttop_adcode'] != '') { ?>
									<?php $toptime = $mts_options['mts_posttop_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$toptime day")), get_the_time("Y-m-d") ) >= 0) { ?>
										<div class="topad">
											<?php echo do_shortcode($mts_options['mts_posttop_adcode']); ?>
										</div>
									<?php } ?>
								<?php } ?>
								<?php if($mts_options['mts_social_buttons'] == '1' && $mts_options['mts_social_button_position'] == '1') { ?>
	                    			<!-- Start Share Buttons -->
	                    			<div class="shareit top">
	                    				<?php if($mts_options['mts_twitter'] == '1') { ?>
	                    					<!-- Twitter -->
	                    					<span class="share-item twitterbtn">
	                    						<a href="https://twitter.com/share" class="twitter-share-button" data-via="<?php echo $mts_options['mts_twitter_username']; ?>">Tweet</a>
	                    					</span>
	                    				<?php } ?>
	                    				<?php if($mts_options['mts_gplus'] == '1') { ?>
	                    					<!-- GPlus -->
	                    					<span class="share-item gplusbtn">
	                    						<g:plusone size="medium"></g:plusone>
	                    					</span>
	                    				<?php } ?>
	                    				<?php if($mts_options['mts_facebook'] == '1') { ?>
	                    					<!-- Facebook -->
	                    					<span class="share-item facebookbtn">
	                    						<div id="fb-root"></div>
	                    						<div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
	                    					</span>
	                    				<?php } ?>
	                    				<?php if($mts_options['mts_linkedin'] == '1') { ?>
	                    					<!--Linkedin -->
	                    					<span class="share-item linkedinbtn">
	                    						<script type="IN/Share" data-url="<?php get_permalink(); ?>"></script>
	                    					</span>
	                    				<?php } ?>
	                    				<?php if($mts_options['mts_stumble'] == '1') { ?>
	                    					<!-- Stumble -->
	                    					<span class="share-item stumblebtn">
	                    						<su:badge layout="1"></su:badge>
	                    					</span>
	                    				<?php } ?>
	                    				<?php if($mts_options['mts_pinterest'] == '1') { ?>
	                    					<!-- Pinterest -->
	                    					<span class="share-item pinbtn">
	                    						<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>" class="pin-it-button" count-layout="horizontal">Pin It</a>
	                    					</span>
	                    				<?php } ?>
	                    			</div>
	                    			<!-- end Share Buttons -->
	                    		<?php } ?>
								<?php the_content(); ?>
								<?php wp_link_pages(array('before' => '<div class="pagination">', 'after' => '</div>', 'link_before'  => '<span class="current"><span class="currenttext">', 'link_after' => '</span></span>', 'next_or_number' => 'next_and_number', 'nextpagelink' => __('Next','mythemeshop'), 'previouspagelink' => __('Previous','mythemeshop'), 'pagelink' => '%','echo' => 1 )); ?>
								<?php if ($mts_options['mts_postend_adcode'] != '') { ?>
									<?php $endtime = $mts_options['mts_postend_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$endtime day")), get_the_time("Y-m-d") ) >= 0) { ?>
										<div class="bottomad">
											<?php echo do_shortcode($mts_options['mts_postend_adcode']); ?>
										</div>
									<?php } ?>
								<?php } ?> 
								<?php if($mts_options['mts_social_buttons'] == '1' && $mts_options['mts_social_button_position'] != '1') { ?>
									<!-- Start Share Buttons -->
									<div class="shareit">
										<?php if($mts_options['mts_twitter'] == '1') { ?>
											<!-- Twitter -->
											<span class="share-item twitterbtn">
												<a href="https://twitter.com/share" class="twitter-share-button" data-via="<?php echo $mts_options['mts_twitter_username']; ?>">Tweet</a>
											</span>
										<?php } ?>
										<?php if($mts_options['mts_gplus'] == '1') { ?>
											<!-- GPlus -->
											<span class="share-item gplusbtn">
												<g:plusone size="medium"></g:plusone>
											</span>
										<?php } ?>
										<?php if($mts_options['mts_facebook'] == '1') { ?>
											<!-- Facebook -->
											<span class="share-item facebookbtn">
												<div id="fb-root"></div>
												<div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
											</span>
										<?php } ?>
										<?php if($mts_options['mts_linkedin'] == '1') { ?>
											<!--Linkedin -->
											<span class="share-item linkedinbtn">
												<script type="IN/Share" data-url="<?php get_permalink(); ?>"></script>
											</span>
										<?php } ?>
										<?php if($mts_options['mts_stumble'] == '1') { ?>
											<!-- Stumble -->
											<span class="share-item stumblebtn">
												<su:badge layout="1"></su:badge>
											</span>
										<?php } ?>
										<?php if($mts_options['mts_pinterest'] == '1') { ?>
											<!-- Pinterest -->
											<span class="share-item pinbtn">
												<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>" class="pin-it-button" count-layout="horizontal">Pin It</a>
											</span>
										<?php } ?>
									</div>
									<!-- end Share Buttons -->
								<?php } ?>
								<?php if($mts_options['mts_tags'] == '1') { ?>
									<div class="tags"><?php the_tags('<span class="tagtext">'.__('Tags','mythemeshop').':</span>',', ') ?></div>
								<?php } ?>
							</div>
						</div><!--.post-content box mark-links-->
	                    <?php if (!empty($mts_options['mts_prevnext_post_links'])) { ?>
	                    <div class="pagination pagination-single">
	                        <ul>
	                            <li><?php next_post_link('%link', '<i class="fa fa-angle-left"></i> %title'); ?></li>
	                            <li><?php previous_post_link('%link', '<i class="fa fa-angle-right"></i> %title'); ?></li>
	                        </ul>
	                    </div>
	                    <?php } ?>
	                    
	                    <?php if($mts_options['mts_author_box'] == '1') { ?>
							<div class="postauthor">
								<h4><?php _e('About The Author', 'mythemeshop'); ?></h4>
								<div class="author-info">
									<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '100' );  } ?>
									<h5><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="nofollow"><?php the_author_meta( 'nickname' ); ?></a></h5>
									<p><?php the_author_meta('description') ?></p>
								</div>
							</div>
						<?php }?>  

	                    <?php if(!empty($mts_options['mts_related_posts'])) { ?>	
							<!-- Start Related Posts -->
							<?php 
	                        $empty_taxonomy = false;
	                        if (empty($mts_options['mts_related_posts_taxonomy']) || $mts_options['mts_related_posts_taxonomy'] == 'tags') {
	                            // related posts based on tags
	                            $tags = get_the_tags($post->ID); 
	                            if (empty($tags)) { 
	                                $empty_taxonomy = true;
	                            } else {
	                                $tag_ids = array(); 
	                                foreach($tags as $individual_tag) {
	                                    $tag_ids[] = $individual_tag->term_id; 
	                                }
	                                $args = array( 'tag__in' => $tag_ids, 
	                                    'post__not_in' => array($post->ID), 
	                                    'posts_per_page'=>4, 
	                                    'ignore_sticky_posts' => 1, 
	                                    'orderby' => 'rand' 
	                                );
	                            }
	                         } else {
	                            // related posts based on categories
	                            $categories = get_the_category($post->ID); 
	                            if (empty($categories)) { 
	                                $empty_taxonomy = true;
	                            } else {
	                                $category_ids = array(); 
	                                foreach($categories as $individual_category) 
	                                    $category_ids[] = $individual_category->term_id; 
	                                $args = array( 'category__in' => $category_ids, 
	                                    'post__not_in' => array($post->ID), 
	                                    'posts_per_page'=>4, 
	                                    'ignore_sticky_posts' => 1, 
	                                    'orderby' => 'rand' 
	                                );
	                            }
	                         }
	                        if (!$empty_taxonomy) {
							$my_query = new wp_query( $args ); if( $my_query->have_posts() ) {
								echo '<div class="related-posts">';
	                            echo '<h4>'.__('Related Posts','mythemeshop').'</h4>';
	                            echo '<div class="clear">';
	                            if ($article_class == 'article') {
	                                $posts_per_row = 3;
	                            } else {
	                                $posts_per_row = 4;
	                            }
								while( $my_query->have_posts() ) { $my_query->the_post(); ?>
								<article class="latestPost excerpt  <?php echo (++$j % $posts_per_row == 0) ? 'last' : ''; ?>">
	                                <div class="featured-post clearfix">
	                					<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow" id="featured-thumbnail"<?php if (mts_get_category_color()) { echo ' style="border-color: '.mts_get_category_color().';"'; } ?>>
	                						<?php if ( has_post_thumbnail() ) { ?>
	                							<?php echo '<div class="featured-thumbnail">'; the_post_thumbnail('featured',array('title' => '')); echo '</div>'; ?>
	                						<?php } else { ?>
	                							<div class="featured-thumbnail">
	                								<img width="223" height="137" src="<?php echo get_template_directory_uri(); ?>/images/nothumbfeatured.png" class="attachment-featured wp-post-image" alt="<?php the_title(); ?>">
	                							</div>
	                						<?php } ?>
	                					</a>
	                                    <?php if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
	                                    <div class="categories"<?php if (mts_get_category_color()) { echo ' style="background: '.mts_get_category_color().';"'; } ?>><?php $category = get_the_category(); echo '<a href="'.get_category_link( $category[0]->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", 'mythemeshop' ), $category[0]->name ) ) . '">'.$category[0]->cat_name.'</a>'; ?></div>
	                                </div>
	            					<header>
	                                    <h2 class="title front-view-title">
	        								<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php echo mts_truncate(get_the_title()); ?></a>
	        							</h2>
	            						<?php if($mts_options['mts_home_headline_meta'] == '1') { ?>
	            							<div class="post-info">
	            								<?php if(isset($mts_options['mts_home_headline_meta_info']['author']) == '1') { ?>
	            									<span class="theauthor"><?php _e('By', 'mythemeshop'); ?> <?php the_author_posts_link(); ?></span>
	            								<?php } ?>
	            								<?php if(isset($mts_options['mts_home_headline_meta_info']['date']) == '1') { ?>
	            									<span class="thetime updated"><?php the_time('M n, Y'); ?></span>
	            								<?php } ?>
	            							</div>
	            						<?php } ?>
	            					</header>
	                            </article><!--.post excerpt-->
								<?php } echo '</div></div>'; }} wp_reset_query(); ?>
							<!-- .related-posts -->
						<?php }?> 
					</div><!--.g post-->
					<?php comments_template( '', true ); ?>
				<?php endwhile; /* end loop */ ?>
			</div>
		</article>
		<?php get_sidebar(); ?>
	<?php get_footer(); ?>