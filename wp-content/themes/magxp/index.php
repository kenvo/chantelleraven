<?php get_header(); ?> 
    <div class="event-container">
    <?php
        $events = tribe_get_events( array(
            'posts_per_page' => 9
        ) );

        foreach( $events as $event ) {
    ?>
            <div class="event-item" style="background: url( '<?php echo( get_the_post_thumbnail_url( $event->ID, 'full' ) ); ?>' );">
                <a href="<?php echo( $event->guid ); ?>">
                    <div class="event-item-container">
                    </div>
                    <div class="event-name">
                        <h1><?php echo( $event->post_title ); ?></h1>
                    </div>
                    <div class="event-place">
                        <p>
                            <span class="tag <?php echo( wp_get_post_terms( $event->ID, 'tribe_events_cat' )[0]->name ); ?>"><?php echo( wp_get_post_terms( $event->ID, 'tribe_events_cat' )[0]->name ); ?></span>
                            <?php echo( tribe_get_start_date( $event ) ); ?>
                        </p>
                        <p>
                            <i class="fa fa-map-marker" aria-hidden="true"></i> 
                            <?php echo( 
                                    trim( tribe_get_venue( $event->ID ) ) != '' ? 
                                    tribe_get_venue( $event->ID ) : 'Comming Soon!!!' 
                                ); 
                            ?>
                        </p>
                    </div>
                </a>
            </div>
    <?php
        }
    ?>
    </div>
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous">
    </script>

    <script>
    
    </script>

<?php get_footer(); ?>