<?php $mts_options = get_option('magxp'); ?>
<?php get_header(); ?>

<div class="main-container">
    <div id="page">
        <?php 
        if (empty($mts_options['mts_homepage_layout'])) {
            $layout = 'grid';
            $thumb = array('w' => 203, 'h' => 150, 'size' => 'featured');
        } else {
            $layout = 'blog';
            $thumb = array('w' => 634, 'h' => 280, 'size' => 'featured-blog');
        }
        ?>
        <div class="article">
            <div id="content_box">
                <h1 class="postsby">
                    <?php if (is_category()) { ?>
                        <span><?php single_cat_title(); ?><?php _e(" Archive", "mythemeshop"); ?></span>
                    <?php } elseif (is_tag()) { ?> 
                        <span><?php single_tag_title(); ?><?php _e(" Archive", "mythemeshop"); ?></span>
                    <?php } elseif (is_author()) { ?>
                        <span><?php  $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); echo $curauth->nickname; _e(" Archive", "mythemeshop"); ?></span> 
                    <?php } elseif (is_day()) { ?>
                        <span><?php _e("Daily Archive:", "mythemeshop"); ?></span> <?php the_time('l, F j, Y'); ?>
                    <?php } elseif (is_month()) { ?>
                        <span><?php _e("Monthly Archive:", "mythemeshop"); ?>:</span> <?php the_time('F Y'); ?>
                    <?php } elseif (is_year()) { ?>
                        <span><?php _e("Yearly Archive:", "mythemeshop"); ?>:</span> <?php the_time('Y'); ?>
                    <?php } ?>
                </h1>
                <div class="clear">
                    <?php $j = 0; if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <article class="latestPost excerpt <?php echo (++$j % 3 == 0) ? 'last' : ''; ?>">
                            <div class="featured-post clearfix">
                                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow" id="featured-thumbnail"<?php if (mts_get_category_color()) { echo ' style="border-color: '.mts_get_category_color().';"'; } ?>>
                                    <?php if ( has_post_thumbnail() ) { ?>
                                        <?php echo '<div class="featured-thumbnail">'; the_post_thumbnail($thumb['size'],array('title' => '')); echo '</div>'; ?>
                                    <?php } else { ?>
                                        <div class="featured-thumbnail">
                                            <img width="<?php echo $thumb['w']; ?>" height="<?php echo $thumb['h']; ?>" src="<?php echo get_template_directory_uri(); ?>/images/nothumb<?php echo $thumb['size']; ?>.png" class="attachment-featured wp-post-image" alt="<?php the_title(); ?>">
                                        </div>
                                    <?php } ?>
                                </a>
                                <?php if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
                                <div class="categories"<?php if (mts_get_category_color()) { echo ' style="background: '.mts_get_category_color().';"'; } ?>><?php $category = get_the_category(); echo '<a href="'.get_category_link( $category[0]->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", 'mythemeshop' ), $category[0]->name ) ) . '">'.$category[0]->cat_name.'</a>'; ?></div>
                            </div>
                            <header>
                                <h2 class="title front-view-title">
                                    <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php echo mts_truncate(get_the_title()); ?></a>
                                </h2>
                                <?php if($mts_options['mts_home_headline_meta'] == '1') { ?>
                                    <div class="post-info">
                                        <?php if(isset($mts_options['mts_home_headline_meta_info']['author']) == '1') { ?>
                                            <span class="theauthor"><?php _e('By', 'mythemeshop'); ?> <?php the_author_posts_link(); ?></span>
                                        <?php } ?>
                                        <?php if(isset($mts_options['mts_home_headline_meta_info']['date']) == '1') { ?>
                                            <span class="thetime updated"><?php the_time('M j, Y'); ?></span>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </header>
                            <?php if ($layout == 'blog') : ?>
                                <?php if (empty($mts_options['mts_full_posts'])) : ?>
                                    <div class="front-view-content">
                                        <?php echo mts_excerpt(29); ?>
                                    </div>
                                    <div class="readMore"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow"><?php _e('Read More','mythemeshop'); ?></a></div>
                                <?php else : ?>
                                    <div class="front-view-content full-post">
                                        <?php the_content(); ?>
                                    </div>
                                    <?php if (mts_post_has_moretag()) : ?>
                                        <div class="readMore"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="nofollow"><?php _e('Read More','mythemeshop'); ?></a></div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </article><!--.post excerpt-->
                    <?php endwhile; endif; ?>
                </div>
            </div>
                <!--Start Pagination-->
                <?php if (isset($mts_options['mts_pagenavigation_type']) && $mts_options['mts_pagenavigation_type'] == '1' ) { ?>
                    <?php $additional_loop = 0; mts_pagination($additional_loop['max_num_pages']); ?> 
                <?php } else { ?>
                    <div class="pagination">
                        <ul>
                            <li class="nav-previous"><?php next_posts_link( __( '&larr; '.'Older posts', 'mythemeshop' ) ); ?></li>
                            <li class="nav-next"><?php previous_posts_link( __( 'Newer posts'.' &rarr;', 'mythemeshop' ) ); ?></li>
                        </ul>
                    </div>
                <!--End Pagination-->
            <?php } ?>  
        </div>
        <?php get_sidebar(); ?>
    <?php get_footer(); ?>