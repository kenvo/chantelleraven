<?php
/**
 * @package 
 * @version 
 */
/*
Plugin Name: Event commerce
Plugin URI: #
Description: Plugin use for make use of the event calendar like a woocommerce product
Author: Bui Khoi
Version: 1
Author URI: #
*/

class event_commerce {
    
    private static $instance;
    private $plugin_dir;
    private $plugin_url;

    //-----------------------------
    // Check if a instance exists
    //-----------------------------
    public static function get_instance() {

        // Check if a instace of this class is existed?
        if( static::$instance == null ) {
            static::$instance = new static();
        }
    }

    //-----------------------------
    // Constructor
    //-----------------------------
    public function __construct() {
        $this->plugin_dir = plugin_dir_path( __FILE__ );
        $this->plugin_url = plugin_dir_url( __FILE__ );

        require_once( $this->plugin_dir . 'class/event-product.php' );

        // Register function for active plugin
        register_activation_hook( __FILE__, array( $this, 'active_plugin' ) );

        //Register function for deactive plugin
        register_deactivation_hook( __FILE__, array( $this, 'disable_plugin' ) );

        new Event_Product( $this->plugin_url );

        //Add filter to change template directory for tribe_events(The event calendar)
        add_filter( 'template_include', array( $this, 'change_tribe_event_template' ), 30 );

        //Enqueue css and javascript for front end
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_event_css_js' ) );

        //Add setting to settings in dashboard
        add_action( 'admin_init', array( $this, 'create_setting_page' ) );

        //Add shortcode
        add_action( 'init', array( $this, 'add_list_event_shortcode' ) );

    }

    //--------------------------------
    // Function use for active plugin
    //--------------------------------
    public function active_plugin()  {
        
    }

    //--------------------------------
    // Function use for disable plugin
    //--------------------------------
    public function disable_plugin() {
        unregister_setting( 'general', 'khoi_event_commerce_page_id' );
    }

    //----------------------------------------------------------
    // Function use for change the template directory for events
    //----------------------------------------------------------
    public function change_tribe_event_template( $template ) {
        global $post;

        if( isset( $post ) ) {
            $single_template_dir = $this->plugin_dir . 'view/single-event.php';
            $archive_template_dir = $this->plugin_dir . 'view/archive_event.php';
            $buy_event_template_dir = $this->plugin_dir . 'view/buy-event.php';
            $archive_page_id = get_option( 'khoi_event_commerce_page_id' );
            $buy_event_page_id = get_option( 'khoi_event_commerce_buy_page_id' );

            if( is_single() && $post->post_type == 'tribe_events' && file_exists( $single_template_dir ) ) {
                $template = $single_template_dir;
            } elseif ( 
                intval( $archive_page_id ) > -1 && 
                is_page( $archive_page_id ) &&
                file_exists( $archive_template_dir )
            ) {
                $template = $archive_template_dir; 
            } elseif ( 
                intval( $buy_event_page_id ) > -1 && 
                is_page( $buy_event_page_id ) &&
                file_exists( $archive_template_dir )
            ) {
                $template = $buy_event_template_dir; 
            }
        }

        return $template;
    }

    //---------------------------------------------------------------
    // Function use for enqueue css and js for single vs archive page
    //---------------------------------------------------------------
    public function enqueue_event_css_js() {
        global $post;
        $archive_page_id = get_option( 'khoi_event_commerce_page_id' );
        $buy_event_page_id = get_option( 'khoi_event_commerce_buy_page_id' );

        if( is_single() && $post->post_type == 'tribe_events' ) {
            wp_enqueue_style( 'single_event_font_awesome_css', $this->plugin_url . 'css/font-awesome.min.css' );
            wp_enqueue_style( 'single_event_main_css', $this->plugin_url . 'css/single-event.css' );
            wp_enqueue_style( 'archive_event_main_css', $this->plugin_url . 'css/archive-event.css' );
        } elseif ( 
            intval( $archive_page_id ) > -1 && 
            is_page( $archive_page_id ) ||
            intval( $buy_event_page_id ) > -1 && 
            is_page( $buy_event_page_id )
        ) {
            wp_enqueue_style( 'single_event_main_css', $this->plugin_url . 'css/single-event.css' );
            wp_enqueue_style( 'single_event_font_awesome_css', $this->plugin_url . 'css/font-awesome.min.css' );
            wp_enqueue_style( 'archive_event_main_css', $this->plugin_url . 'css/archive-event.css' );
        }
    }

    //-------------------------------------------
    // Function use for create setting for plugin
    //-------------------------------------------
    public function create_setting_page() {
        register_setting(
            'general',
            'khoi_event_commerce_page_id',
            array(
                'default' => -1
            )
        );

        register_setting(
            'general',
            'khoi_event_commerce_buy_page_id',
            array(
                'default' => -1
            )
        );

        add_settings_section( 
            'khoi_event_commerce_setting', 
            '', 
            array( $this, 'render_setting_section' ),
            'general'
        );

        add_settings_field(
            'khoi_event_commerce_setting_field',
            'page id',
            array( $this, 'render_setting_field' ),
            'general',
            'khoi_event_commerce_setting'
        );
    }

    public function render_setting_section() {
        echo( '<h1>Event Commerce</h1>' );
    }

    public function render_setting_field() {
        echo( '<p>Page id for archive event</p>' );
        echo( '<input type="text" name="khoi_event_commerce_page_id" value="' . get_option( 'khoi_event_commerce_page_id' ) . '"/>' );
        echo( '<p>Page id for buy event</p>' );
        echo( '<input type="text" name="khoi_event_commerce_buy_page_id" value="' . get_option( 'khoi_event_commerce_buy_page_id' ) . '"/>' );
    }

    //-------------------------------------------
    // Function use for add shortcode
    //-------------------------------------------
    public function list_event_shortcode() {
        global $wpdb;
        $events_id_list = $wpdb->get_col("
            SELECT " . $wpdb->postmeta . ".post_id
            FROM " . $wpdb->postmeta . "
            INNER JOIN " . $wpdb->posts . "
            ON " . $wpdb->postmeta . ".post_id = " . $wpdb->posts . ".ID
            WHERE " . $wpdb->postmeta . ".meta_key = '_EventStartDate'
            AND " . $wpdb->posts . ".post_type = 'tribe_events'
            ORDER BY STR_TO_DATE(meta_value,'%Y-%m-%d %H:%i:%s') DESC
            LIMIT 10
        ");

        $events = [];

        foreach ($events_id_list as $event_id) {
            $post = get_post($event_id);

            if (strcmp($post->post_type, 'tribe_events') == 0) {
                $events[] = $post;
            }
        }

        $past_event = [];
        $future_event = [];

        foreach ($events as $event) {
            if (strtotime(tribe_get_start_date($event, false, 'Y-m-d')) >= strtotime(date('Y-m-d'))) {
                $future_event[] = $event;
            } else {
                $past_event[] = $event;
            }
        }

        $events = array_merge(array_reverse($future_event), $past_event);
        $events = array_slice($events, 0, 4);

        require_once( $this->plugin_dir . 'view/list-event.php' );
    }

    public function add_list_event_shortcode() {
        add_shortcode( 'event_commerce', array( $this, 'list_event_shortcode' ) );
    }
}

// Start the plugin by create new instace
if( class_exists( 'event_commerce' ) ) {
    event_commerce::get_instance();
}