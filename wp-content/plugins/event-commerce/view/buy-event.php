<?php
	$event = $post = get_post( $_GET['event'] );
	$product = wc_get_product( $_GET['event'] );
	$redirect_link = trim(get_field( 'redirect_link', $event->ID ));
	$earlier_event = tribe_get_events( array(
	 	'posts_per_page' => 5,
	 	'post__not_in' => array( $event->ID ),
	 	'end_date' => tribe_get_start_date( $event, true, $format = 'yyyy-mm-dd hh:mm' ),
	 	'orderby' => 'DATE',
	 	'order' => 'DESC'
	) );
  add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
 
	function woo_custom_cart_button_text() {
    return __( 'Continue to payment', 'woocommerce' );	 
	}
	$later_event = tribe_get_events( array(
	 	'posts_per_page' => 1,
	 	'post__not_in' => array( $event->ID ),
	 	'start_date' => tribe_get_end_date( $event, true, $format = 'yyyy-mm-dd hh:mm' ),
	 	'orderby' => 'DATE',
	 	'order' => 'DESC'
	) );

	get_header();

	$post = $event;
?>
	<div class="event-single">
		<div 
			class="event-single-top-container" 
			style="background: url( '<?php echo( get_the_post_thumbnail_url( $event->ID, 'full' ) ); ?>' );">
		</div>
		<div class="event-single-bottom-container">
			<div class="event-single-description">
				<h1><?php echo( $event->post_title ); ?></h1>
				<div class="form-container">
					<?php 
						$field = get_field_object('form_survey');
						$value = $field['value'];
						$label = $field['choices'][ $value ];
						$form_id = str_replace( 'formsurvey', '', $value );
						echo do_shortcode('[gravityform id="'. $form_id .'" title="false" description="false" ajax="true"]');
						if (strlen(trim($redirect_link)) === 0) {
							echo('<h2>Your investment</h2>');
							echo('<p style="margin-bottom:-7px;">Please select to pay the deposit or full amount</p>');
							do_action( 'woocommerce_single_product_summary' );
						} else {
							echo('
								<div class="gem-button-container gem-button-position-inline"><button class="gem-button gem-button-size-small gem-button-style-flat gem-button-text-weight-normal gem-button-icon-position-left single_add_to_cart_button button alt" style="border-radius: 3px;background-color: #5b196f;" onmouseleave="this.style.backgroundColor="0#5b196f";" onmouseenter="" type="submit"><i class="gem-print-icon gem-icon-pack-thegem-icons gem-icon-cart"></i>Continue to Eventbrite</button></div>
							');
						}
					?>
				</div>
			</div>
			<div class="event-single-location">
				<ul>
					<li>
						<h6>Date </h6>
						<h4><?php echo( tribe_get_start_date( $event, false, $format = 'F d, Y' ) ); ?></h4>
						<p>
							<?php echo( tribe_get_start_date( $event, false, $format = 'D' ) . ' until ' . tribe_get_end_date( $event, false, $format = 'F d, Y' ) ); 
							?>
						</p>
					</li>
					<li>
						<h6>Time </h6>
						<h4>
							<?php echo( tribe_get_start_date( $event, false, $format = 'G:i a' ) . ' - ' . tribe_get_end_date( $event, false, $format = 'G:i a' ) ); ?>
						</h4>
					</li>
					<li>
						<h6>Location </h6>
						<h4>
						<?php 
							echo( 
								trim( tribe_get_venue( $event ) ) != '' ?
								tribe_get_venue( $event ) : 'Comming Soon!!!'
							); 
						?>
						</h4>
						<p>
						<?php
							echo(
								trim( tribe_get_venue( $event ) ) != '' ?
								'at ' . tribe_get_address( $event ) : ''
							);
						?>	
						</p>
					</li>
				</ul>
			</div>
			<div class="button-container">
				<?php
					if( count( $earlier_event ) > 0 ) {
						echo( '
							<div class="prev-btn">
								<a href="' . $earlier_event[0]->guid . '">
									<i class="fa fa-long-arrow-left" aria-hidden="true"></i> EALIER( ' . 
										tribe_get_start_date( $earlier_event[0], false, $format = 'G:i A' ) .
										' '.
										$earlier_event[0]->post_title .
									' )
								</a>
							</div>
						' );
					}

					if( count( $later_event ) > 0 ) {
						echo( '
							<div class="next-btn">
								<a href="' . $later_event[0]->guid . '">
									LATER( ' . 
										tribe_get_start_date( $later_event[0], false, $format = 'G:i A' ) .
										' '.
										$later_event[0]->post_title .
									' )
									<i class="fa fa-long-arrow-right" aria-hidden="true"></i> 
								</a>
							</div>
						' );
					}
				?>
			</div>
		</div>
	</div>
<?php

	get_footer();
?>
<script>
jQuery( document ).ready( function() {
	var redirect_link = '<?php echo($redirect_link) ?>';
	isFormSubmitButtonClicked = 0;

	jQuery( '.event-single .form-container button[type="submit"]' ).click( function( event ) { 
		event.preventDefault(); 
		
		jQuery( 'input[type="submit"].gform_button.button' ).click();
		if( isFormSubmitButtonClicked != 1 ) {
			var check = setInterval( function() {
				if( !jQuery( 'input[type="submit"].gform_button.button' ).length ) {
					jQuery( '.event-single .form-container button[type="submit"]' ).hide();
					// Check if redirect link exist
					if (
						typeof(redirect_link) === 'string'
						&& redirect_link.trim()
					) {
						window.location.href = redirect_link;
					} else {
						jQuery( 'form.cart' ).submit();
					}

					clearInterval( check );
				}
			}, 500 );
		}

		isFormSubmitButtonClicked = 1;
	} );

	var deposit_price_text = jQuery( '#wc-deposits-options-form' ).find( '.deposit-option' ).text();
	var full_amount_text = 'Full amount: ' + jQuery( 'p.price' ).find( '.woocommerce-Price-amount' ).text() + ' per item';

	jQuery( '#wc-deposits-options-form' ).find( 'label' ).click( function() {
		if( jQuery( this ).attr( 'for' ) == 'pay-deposit' ) {
			jQuery( '#wc-deposits-options-form' ).find( '.deposit-option' ).text( deposit_price_text );
		} else {
			jQuery( '#wc-deposits-options-form' ).find( 'label.deposit-option' ).text( full_amount_text );
		}
	} );
} );
</script>
