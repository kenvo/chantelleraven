<?php
	global $post;
	$event = $post;
	$product = wc_get_product( $post->ID );
	$redirect_link = trim(get_field( 'redirect_link', $event->ID ));
  $event_image = trim(get_field( 'event_image', $event->ID ));
  if (strlen($event_image) === 0) {
    $event_image = get_the_post_thumbnail_url( $event->ID, 'full' );
  }

	$earlier_event = tribe_get_events( array(
	 	'posts_per_page' => 5,
	 	'post__not_in' => array( $event->ID ),
	 	'end_date' => tribe_get_start_date( $event, true, $format = 'yyyy-mm-dd hh:mm' ),
	) );

	$later_event = tribe_get_events( array(
	 	'posts_per_page' => 1,
	 	'post__not_in' => array( $event->ID ),
	 	'start_date' => tribe_get_end_date( $event, true, $format = 'yyyy-mm-dd hh:mm' ),
	) );

	get_header();

	$post = $event;
?>
	<div class="event-single">
		<div 
			class="event-single-top-container" 
			style="background: url( '<?php echo($event_image); ?>' );">
		</div>
		<div class="event-single-bottom-container">
			<div class="event-single-description">
				<h1><?php echo( $event->post_title ); ?></h1>
				<p><?php echo wpautop( $event->post_content ) ?></p>
				<div class="form-container">
					<!--<a 
						class="sign-up-btn" 
						href="<?php echo( 
							add_query_arg( 
								'event',
								$post->ID,
								get_page_link( get_option( 'khoi_event_commerce_buy_page_id' ) )
							)
						);
						?>"
					>
						SECURE YOUR PLACE
					</a>-->
					<a 
						class="sign-up-btn" 
						href="<?php echo($redirect_link);?>"
					>
						SECURE YOUR PLACE
					</a>
				</div>
			</div>
			<div class="event-single-location">
				<ul>
					<li>
						<h6>Date </h6>
						<h4><?php echo( tribe_get_start_date( $event, false, $format = 'F d, Y' ) ); ?></h4>
						<p>
							<?php echo( tribe_get_start_date( $event, false, $format = 'D' ) . ' until ' . tribe_get_end_date( $event, false, $format = 'F d, Y' ) ); 
							?>
						</p>
					</li>
					<li>
						<h6>Time </h6>
						<h4>
							<?php echo( tribe_get_start_date( $event, false, $format = 'G:i a' ) . ' - ' . tribe_get_end_date( $event, false, $format = 'G:i a' ) ); ?>
						</h4>
					</li>
					<li>
						<h6>Location </h6>
						<h4>
						<?php 
							echo( 
								trim( tribe_get_venue( $event ) ) != '' ?
								tribe_get_venue( $event ) : 'Comming Soon!!!'
							); 
						?>
						</h4>
						<p>
						<?php
							echo(
								trim( tribe_get_venue( $event ) ) != '' ?
								'at ' . tribe_get_address( $event ) : ''
							);
						?>	
						</p>
					</li>
				</ul>
				<div class="form-container">
					<!--<a 
						class="sign-up-btn" 
						href="<?php echo( 
							add_query_arg( 
								'event',
								$post->ID,
								get_page_link( get_option( 'khoi_event_commerce_buy_page_id' ) )
							)
						);
						?>"
					>
						SECURE YOUR PLACE
					</a>-->
					<a 
						class="sign-up-btn" 
						href="<?php echo($redirect_link);?>"
					>
						SECURE YOUR PLACE
					</a>
				</div>
			</div>
			<div class="button-container">
				<?php
					if( count( $earlier_event ) > 0 ) {
						echo( '
							<div class="prev-btn">
								<a href="' . $earlier_event[0]->guid . '">
									<i class="fa fa-long-arrow-left" aria-hidden="true"></i> EALIER( ' . 
										tribe_get_start_date( $earlier_event[0], false, $format = 'G:i A' ) .
										' '.
										$earlier_event[0]->post_title .
									' )
								</a>
							</div>
						' );
					}

					if( count( $later_event ) > 0 ) {
						echo( '
							<div class="next-btn">
								<a href="' . $later_event[0]->guid . '">
									LATER( ' . 
										tribe_get_start_date( $later_event[0], false, $format = 'G:i A' ) .
										' '.
										$later_event[0]->post_title .
									' )
									<i class="fa fa-long-arrow-right" aria-hidden="true"></i> 
								</a>
							</div>
						' );
					}
				?>
			</div>
		</div>
	</div>
<?php
	get_footer();