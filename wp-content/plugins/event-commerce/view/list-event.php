<div class="vc_row wpb_row vc_row-fluid vc_custom_1484712468105 vc_row-has-fill">
	<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-12 vc_col-md-12">
		<div class="vc_column-inner" style="background: white;">
			<div class="wpb_wrapper">
				<div class="blog blog-style-compact clearfix item-animation-move-up" data-next-page="0"> 
					<?php 
						foreach( $events as $event ) { 
					?>
<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-6">
							<!-- START OF AN ARTICLE -->
							<article class="no-image clearfix post-26205 post type-post status-publish format-standard has-post-thumbnail hentry category-news_post item-animations-inited start-animation" style="min-height: 200px; margin-bottom: 20px;"> 
								<div class="gem-compact-item-left"> 
									<div class="gem-compact-item-image"> 
										<a 
											class="default" 
											href="<?php echo( $event->guid ); ?>"
										>
											<img 
												width="366" 
												height="296" 
												src="<?php echo( get_the_post_thumbnail_url( $event->ID, 'full' ) ); ?>" 
												class="img-responsive wp-post-image" alt="NATALIE ANN PHOTOGRAPHY"
											>
										</a> 
									</div> 
								</div> 
								<div class="gem-compact-item-right"> 
									<div class="gem-compact-item-content"> 
										<div class="post-title"> 
											<h5 class="entry-title reverse-link-color">
												<a href="<?php echo( $event->guid ); ?>" rel="bookmark">
													<?php 
														echo( tribe_get_start_date( $event, false, $format = 'd M' ) ) 
													?>: 
													<span class="light">
														<?php echo( $event->post_title ); ?>
													</span>
												</a>
											</h5> 
										</div> 
										<div class="post-text"> 
											<div class="summary"> 
												<p>
													<?php 
														echo( 
															wp_strip_all_tags( substr( $event->post_content, 0, 100 ) . '...' ) 
														); 
													?>
												</p> 
											</div> 
										</div> 
									</div> 
									<div class="post-meta date-color"> 
										<div class="entry-meta clearfix gem-post-date"> 
											<div class="post-meta-right"> 
											</div> 
											<div class="post-meta-left"> 
											</div> 
										</div>
										<!-- .entry-meta --> 
									</div> 
								</div> 
							</article>
							<!-- END OF AN ARTICLE -->
</div>
					<?php 
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>