<?php

global $wpdb;

$events_quantity = intval($wpdb->get_col("
    SELECT count(" . $wpdb->postmeta . ".post_id )
    FROM " . $wpdb->postmeta . "
    INNER JOIN " . $wpdb->posts . "
    ON " . $wpdb->postmeta . ".post_id = " . $wpdb->posts . ".ID
    WHERE " . $wpdb->postmeta . ".meta_key = '_EventStartDate'
    AND " . $wpdb->posts . ".post_type = 'tribe_events'
")[0]);
$page_count = 1;

if (!isset($_GET['pagination']) || !is_numeric($_GET['pagination'])) {
    $pagination = 1;
} else {
    $pagination = $_GET['pagination'];
}

if ($events_quantity % 10 == 0) {
    $page_count = intval($events_quantity / 10);
} else {
    $page_count = intval($events_quantity / 10) + 1;
}

if ($pagination > $page_count) {
    $pagination = 1;
}

$events_id_list = $wpdb->get_col("
    SELECT * FROM (SELECT " . $wpdb->postmeta . ".post_id, STR_TO_DATE(" . $wpdb->postmeta . ".meta_value,'%Y-%m-%d %H:%i:%s') as Date,
    DATEDIFF( STR_TO_DATE(" . $wpdb->postmeta . ".meta_value,'%Y-%m-%d %H:%i:%s') , CURDATE() ) as date2
    FROM " . $wpdb->postmeta . "
    INNER JOIN " . $wpdb->posts . "
    ON " . $wpdb->postmeta . ".post_id = " . $wpdb->posts . ".ID
    WHERE " . $wpdb->postmeta . ".meta_key = '_EventStartDate'
    AND " . $wpdb->posts . ".post_type = 'tribe_events'
    AND STR_TO_DATE(" . $wpdb->postmeta . ".meta_value,'%Y-%m-%d %H:%i:%s') >= CURDATE() ORDER BY Date ASC) AS feature
    UNION 
    SELECT * FROM (SELECT " . $wpdb->postmeta . ".post_id, STR_TO_DATE(" . $wpdb->postmeta . ".meta_value,'%Y-%m-%d %H:%i:%s') as Date,
    DATEDIFF( CURDATE()  + INTERVAL 100 YEAR , STR_TO_DATE(" . $wpdb->postmeta . ".meta_value,'%Y-%m-%d %H:%i:%s') )  as date2
    FROM " . $wpdb->postmeta . "
    INNER JOIN " . $wpdb->posts . "
    ON " . $wpdb->postmeta . ".post_id = " . $wpdb->posts . ".ID
    WHERE " . $wpdb->postmeta . ".meta_key = '_EventStartDate'
    AND " . $wpdb->posts . ".post_type = 'tribe_events'
    AND STR_TO_DATE(" . $wpdb->postmeta . ".meta_value,'%Y-%m-%d %H:%i:%s') < CURDATE()  ORDER BY Date DESC) AS past
    ORDER By date2
    LIMIT 10 OFFSET " . ($pagination - 1) * 10, 0);

$events = [];

foreach ($events_id_list as $event_id) {
    $post = get_post($event_id);

    if (strcmp($post->post_type, 'tribe_events') == 0) {
        $events[] = $post;
    }

}

get_header();

?>
    <div id="page-title" class="page-title-block page-title-alignment-center page-title-style-1 ">
        <div class="container">
            <div class="page-title-title">
                <h1>Events</h1>
            </div>
        </div>
        <div class="breadcrumbs-container">
            <div class="container">
                <div class="breadcrumbs">
                    <span>
                        <a href="<?php echo ($get_home_url); ?>" itemprop="url">
                            <span itemprop="title">Home</span>
                        </a>
                    </span>
                    <span class="divider">
                        <span class="bc-devider"></span>
                    </span>
                    <span class="current">Events</span>
                </div><!-- .breadcrumbs -->
            </div>
        </div>
    </div>
    <div class="event-container">
    <?php

    $condition = true;
    $past_event = [];
    $future_event = [];

    foreach ($events as $event) {
        if (strtotime(tribe_get_start_date($event, false, 'Y-m-d')) >= strtotime(date('Y-m-d'))) {
            $future_event[] = $event;
        } else {
            $past_event[] = $event;
        }

    }

    // $events = array_merge(array_reverse($future_event), $past_event);
    $events = array_merge(($future_event), $past_event);

    foreach ($events as $event) {
    ?>
        <?php if (strtotime(tribe_get_start_date($event, false, 'Y-m-d')) > strtotime(date('Y-m-d'))): ?>
            <div class="upcoming-event">
                <div class="event-item clearfix">
                    <div class="event-item-left-col" >
                        <a href="<?php echo ($event->guid); ?>" style="background: url('<?php echo (get_the_post_thumbnail_url($event->ID, 'full')); ?>'); background-size:cover; height: 100% ">
                        </a>
                        <div class="even-item-time">
                            <p class="month"><?php echo (tribe_get_start_date($event, false, $format = 'M')); ?></p>
                            <p class="day"><?php echo (tribe_get_start_date($event, false, $format = 'j')); ?></p>
                            <p class="hours"><?php echo (tribe_get_start_date($event, false, $format = 'g: i A')); ?></p>
                        </div>
                    </div>
                    <div class="event-item-right-col">
                        <a href="<?php echo ($event->guid); ?>">
                            <h1><?php echo ($event->post_title); ?></h1>
                        </a>
                        <div class="event-item-location">
                            <p><?php echo (tribe_get_start_date($event, false, $format = 'l, M j, Y')); ?></p>
                            <p>
                                <?php echo (
                                      tribe_get_start_date($event, false, $format = 'G:i A') . ' - ' . tribe_get_end_date($event, false, $format = 'G:i A')
                                );?>
                            </p>
                            <p>
                                <?php echo (
                                        trim(tribe_get_venue($event->ID)) != '' ?
                                        tribe_get_venue($event->ID) . '<a class="map-link" href="' . tribe_get_map_link($event->ID) . '"> ( map )</a>' :
                                        'Comming Soon!!!'
                                );?>
                            </p>
                        </div>
                        <p>
                            <?php echo (wp_trim_words($event->post_content, 50, ' ...')); ?>
                        </p>
                         <a href="<?php echo ($event->guid); ?>" title="" class="view_button">View Event &rightarrow;</a>
                    </div>
                </div>
            </div>

        <?php else: ?>
            <div class="<?php echo ($condition) ? 'first_past_event' : 'past_event' ?>">
                <div class="event-item clearfix">
                    <div class="event-item-left-col" >
                        <a href="<?php echo ($event->guid); ?>" style="background: url('<?php echo (get_the_post_thumbnail_url($event->ID, 'full')); ?>'); background-size:cover; height: 100%; background-position: center ">

                        </a>
                        <div class="even-item-time past">
                            <p class="month"><?php echo (tribe_get_start_date($event, false, $format = 'M')); ?></p>
                            <p class="day"><?php echo (tribe_get_start_date($event, false, $format = 'j')); ?></p>
                            <p class="hours"><?php echo (tribe_get_start_date($event, false, $format = 'g: i A')); ?></p>
                        </div>
                    </div>
                    <div class="event-item-right-col">
                        <a href="<?php echo ($event->guid); ?>">
                            <h1><?php echo ($event->post_title); ?></h1>
                        </a>
                        <div class="event-item-location">
                            <p><?php echo (tribe_get_start_date($event, false, $format = 'l, M j, Y')); ?></p>
                            <p>
                                <?php echo (
                                  tribe_get_start_date($event, false, $format = 'G:i A') . ' - ' . tribe_get_end_date($event, false, $format = 'G:i A')
                                );?>
                            </p>
                            <p>
                                <?php echo (
                                    trim(tribe_get_venue($event->ID)) != '' ?
                                    tribe_get_venue($event->ID) . '<a class="map-link" href="' . tribe_get_map_link($event->ID) . '"> ( map )</a>' :
                                    'Comming Soon!!!'
                                );?>
                            </p>
                        </div>
                        <p>
                            <?php echo (wp_trim_words($event->post_content, 50, ' ...')); ?>
                        </p>
                        <a href="<?php echo ($event->guid); ?>" title="" class="view_button">View Event &rightarrow;</a>
                    </div>
                </div>
            </div>
    <?php 
        $condition = false; 
        endif;
    }
    ?>
    </div>
    <div class="pagination">
        <ul>
        <?php
        for ($i = 1; $i <= $page_count; $i++) {
            if ($i == $pagination) {
                echo ('<li><a class="active" href="' . add_query_arg('pagination', $i) . '">' . $i . '</a>');
            } else {
                echo ('<li><a href="' . add_query_arg('pagination', $i) . '">' . $i . '</a>');
            }
        }
        ?>
        </ul>
    </div>
    <!-- 
    <div class="upcoming">
        <div class="event-container">
        <?php
        $upcoming_events = tribe_get_events(array(
            'eventDisplay' => 'upcoming',
            'posts_per_page' => 5,
        ));

        if (count($upcoming_events) > 0) {
            echo ('<h1>' . count($upcoming_events) . ' UPCOMING EVENTS</h1>');
        } else {
            echo ('<h1>THERE IS NO UPCOMING EVENTS</h1>');
        }

        foreach ($upcoming_events as $event) {
        ?>
            <div class="event-item clearfix">
                <div class="event-item-left-col" >
                    <a href="<?php echo ($event->guid); ?>">
                        <img src="<?php echo (get_the_post_thumbnail_url($event->ID, 'full')); ?>" alt="" />
                    </a>
                    <div class="even-item-time">
                        <p class="month"><?php echo (tribe_get_start_date($event, false, $format = 'M')); ?></p>
                        <p class="day"><?php echo (tribe_get_start_date($event, false, $format = 'j')); ?></p>
                        <p class="hours"><?php echo (tribe_get_start_date($event, false, $format = 'g: i A')); ?></p>
                    </div>
                </div>
                <div class="event-item-right-col">
                    <a href="<?php echo ($event->guid); ?>">
                        <h1><?php echo ($event->post_title); ?></h1>
                    </a>
                    <div class="event-item-location">
                        <p><?php echo (tribe_get_start_date($event, false, $format = 'l, M j, Y')); ?></p>
                        <p>
                        <?php echo (
                            tribe_get_start_date($event, false, $format = 'G:i A') . ' - ' . tribe_get_end_date($event, false, $format = 'G:i A')
                        );?>
                        </p>
                        <p>
                        <?php echo (
                            trim(tribe_get_venue($event->ID)) != '' ?
                            tribe_get_venue($event->ID) . '<a href="' . tribe_get_map_link($event->ID) . '"> ( map )</a>' :
                            'Comming Soon!!!'
                        );?>
                        </p>
                    </div>
                    <p>
                    <?php echo (wp_strip_all_tags(substr($event->post_content, 0, 100) . '...')); ?>
                    </p>
                </div>
            </div>
        <?php
        }
        ?>
    -->
    </div>
</div>
<?php
get_footer();
?>